#+TITLE: March 18-April 1, 2022 (15 days)
#+PROPERTY: Effort_ALL 0 0:05 0:10 0:30 1:00 2:00 3:00 4:00
#+COLUMNS: %35ITEM %TASKID %OWNER %3PRIORITY %TODO %5ESTIMATED{+} %3ACTUAL{+}
* REPORTS
** SCRUM BOARD
#+BEGIN: block-update-board
#+END:
** DEVELOPER SUMMARY
#+BEGIN: block-update-summary
#+END:
** BURNDOWN CHART
#+BEGIN: block-update-graph
#+END:
** BURNDOWN LIST
#+PLOT: title:"Burndown" ind:1 deps:(3 4) set:"term dumb" set:"xtics scale 0.5" set:"ytics scale 0.5" file:"burndown.plt" set:"xrange [0:17]"
#+BEGIN: block-update-burndown
#+END:
** TASK LIST
#+BEGIN: columnview :hlines 2 :maxlevel 5 :id "TASKS"
#+END:
* TASKS
  :PROPERTIES:
  :ID:       TASKS
  :SPRINTLENGTH: 15
  :SPRINTSTART: <2022-04-18 Fri>
  :END:
** nihaal
*** DONE Read Linux Weekly News [2/2]
    :PROPERTIES:
    :ESTIMATED: 2
    :ACTUAL:   1.41
    :OWNER: nihaal
    :ID: READ.1647697149
    :TASKID: READ.1647697149
    :END:
    :LOGBOOK:
    CLOCK: [2022-03-31 Thu 16:59]--[2022-03-31 Thu 17:34] =>  0:35
    CLOCK: [2022-03-27 Sun 12:49]--[2022-03-27 Sun 13:07] =>  0:18
    CLOCK: [2022-03-27 Sun 11:16]--[2022-03-27 Sun 11:48] =>  0:32
    :END:
    - [X] Weekly edition for March 17, 2021
    - [X] Weekly edition for March 24, 2021
*** Write post on Syzkaller
    :PROPERTIES:
    :ESTIMATED: 3
    :ACTUAL:
    :OWNER: nihaal
    :ID: WRITE.1647697225
    :TASKID: WRITE.1647697225
    :END:
*** Write post on early userspace
    :PROPERTIES:
    :ESTIMATED: 3
    :ACTUAL:
    :OWNER: nihaal
    :ID: WRITE.1647697551
    :TASKID: WRITE.1647697551
    :END:
    :LOGBOOK:
    CLOCK: [2022-03-19 Sat 20:56]--[2022-03-19 Sat 21:25] =>  0:29
    :END:
*** Write post on ftrace
    :PROPERTIES:
    :ESTIMATED: 3
    :ACTUAL:
    :OWNER: nihaal
    :ID: WRITE.1647697527
    :TASKID: WRITE.1647697527
    :END:
*** DONE Read about Dirty COW and Dirty pipe vulnerabilities [2/2]
    :PROPERTIES:
    :ESTIMATED: 2
    :ACTUAL:   0.87
    :OWNER: nihaal
    :ID: READ.1647697583
    :TASKID: READ.1647697583
    :END:
    :LOGBOOK:
    CLOCK: [2022-03-22 Tue 19:44]--[2022-03-22 Tue 20:36] =>  0:52
    :END:
    - [X] Dirty COW
      - [X] https://lwn.net/Articles/849638/
      - [X] https://lwn.net/Articles/849876/
    - [X] Dirty Pipe
      - [X] https://dirtypipe.cm4all.com/
