#+TITLE: March 4-17, 2022 (14 days)
#+PROPERTY: Effort_ALL 0 0:05 0:10 0:30 1:00 2:00 3:00 4:00
#+COLUMNS: %35ITEM %TASKID %OWNER %3PRIORITY %TODO %5ESTIMATED{+} %3ACTUAL{+}
* REPORTS
** SCRUM BOARD
#+BEGIN: block-update-board
#+END:
** DEVELOPER SUMMARY
#+BEGIN: block-update-summary
#+END:
** BURNDOWN CHART
#+BEGIN: block-update-graph
#+END:
** BURNDOWN LIST
#+PLOT: title:"Burndown" ind:1 deps:(3 4) set:"term dumb" set:"xtics scale 0.5" set:"ytics scale 0.5" file:"burndown.plt" set:"xrange [0:17]"
#+BEGIN: block-update-burndown
#+END:
** TASK LIST
#+BEGIN: columnview :hlines 2 :maxlevel 5 :id "TASKS"
#+END:
* TASKS
  :PROPERTIES:
  :ID:       TASKS
  :SPRINTLENGTH: 14
  :SPRINTSTART: <2022-03-04 Fri>
  :wpd-akshay196: 1
  :wpd-nihaal: 1
  :END:
** akshay196
*** DONE Programming Kubernetes - Part V
    CLOSED: [2022-03-17 Thu 23:31]
    :PROPERTIES:
    :ESTIMATED:   14
    :ACTUAL:   8.95
    :OWNER: akshay196
    :ID: READ.1637477677
    :TASKID: READ.1637477677
    :END:
    :LOGBOOK:
    CLOCK: [2022-03-17 Thu 22:45]--[2022-03-17 Thu 23:31] =>  0:46
    CLOCK: [2022-03-16 Wed 20:11]--[2022-03-16 Wed 21:18] =>  1:07
    CLOCK: [2022-03-15 Tue 20:30]--[2022-03-15 Tue 21:04] =>  0:34
    CLOCK: [2022-03-12 Sat 20:03]--[2022-03-12 Sat 20:51] =>  0:48
    CLOCK: [2022-03-11 Fri 20:44]--[2022-03-11 Fri 21:51] =>  1:07
    CLOCK: [2022-03-10 Thu 20:17]--[2022-03-10 Thu 21:01] =>  0:44
    CLOCK: [2022-03-07 Mon 20:07]--[2022-03-07 Mon 21:28] =>  1:21
    CLOCK: [2022-03-06 Sun 20:51]--[2022-03-06 Sun 21:45] =>  0:54
    CLOCK: [2022-03-05 Sat 17:53]--[2022-03-05 Sat 19:29] =>  1:36
    :END:
    - [X] Chapter 5. Automating Code Generation          (3h)
    - [X] Chapter 6. Solutions for Writing Operators     (6h)
** nihaal
*** DONE Read Linux Weekly News
    :PROPERTIES:
    :ESTIMATED: 2
    :ACTUAL:   1.35
    :OWNER: nihaal
    :ID: READ.1646743529
    :TASKID: READ.1646743529
    :END:
    :LOGBOOK:
    CLOCK: [2022-03-17 Thu 18:49]--[2022-03-17 Thu 19:39] =>  0:50
    CLOCK: [2022-03-12 Sat 18:59]--[2022-03-12 Sat 19:30] =>  0:31
    :END:
    - [X] Weekly edition for March 03, 2021
    - [X] Weekly edition for March 10, 2021
*** Set up Syzkaller and Qemu for fuzzing
    :PROPERTIES:
    :ESTIMATED: 2
    :ACTUAL:   5.10
    :OWNER: nihaal
    :ID: OPS.1646743752
    :TASKID: OPS.1646743752
    :END:
    :LOGBOOK:
    CLOCK: [2022-03-17 Thu 20:29]--[2022-03-17 Thu 21:24] =>  0:55
    CLOCK: [2022-03-17 Thu 20:16]--[2022-03-17 Thu 20:20] =>  0:04
    CLOCK: [2022-03-16 Wed 18:53]--[2022-03-16 Wed 19:36] =>  0:43
    CLOCK: [2022-03-15 Tue 20:42]--[2022-03-15 Tue 21:33] =>  0:51
    CLOCK: [2022-03-14 Mon 21:13]--[2022-03-14 Mon 21:34] =>  0:21
    CLOCK: [2022-03-14 Mon 20:18]--[2022-03-14 Mon 21:13] =>  0:55
    CLOCK: [2022-03-13 Sun 19:55]--[2022-03-13 Sun 20:51] =>  0:56
    CLOCK: [2022-03-12 Sat 21:47]--[2022-03-12 Sat 22:08] =>  0:21
    :END:
*** DONE Learn about Coccinelle
    :PROPERTIES:
    :ESTIMATED: 3
    :ACTUAL:   2.33
    :OWNER: nihaal
    :ID: READ.1646747849
    :TASKID: READ.1646747849
    :END:
    :LOGBOOK:
    CLOCK: [2022-03-11 Fri 19:07]--[2022-03-11 Fri 19:49] =>  0:42
    CLOCK: [2022-03-09 Wed 19:54]--[2022-03-09 Wed 20:50] =>  0:56
    CLOCK: [2022-03-08 Tue 20:05]--[2022-03-08 Tue 20:47] =>  0:42
    :END:
    - [X] [[https://lwn.net/Articles/380835/][Evolutionary development of a semantic patch using Coccinelle]]
    - [X] [[https://lwn.net/Articles/315686/][Semantic patching with Coccinelle]]
    - [X] [[https://coccinelle.gitlabpages.inria.fr/website/papers/ols07-padioleau.pdf][Paper on Coccinelle]]
    - [X] [[https://www.youtube.com/watch?v=Z9FmRyzqyX4][Coccinelle, Prequel, and Spinfer: Code Evolutions in the Linux Kernel]]
