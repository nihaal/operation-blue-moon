#+TITLE: October 7-19, 2021 (13 days)
#+PROPERTY: Effort_ALL 0 0:05 0:10 0:30 1:00 2:00 3:00 4:00
#+COLUMNS: %35ITEM %TASKID %OWNER %3PRIORITY %TODO %5ESTIMATED{+} %3ACTUAL{+}
* REPORTS
** SCRUM BOARD
#+BEGIN: block-update-board
#+END:
** DEVELOPER SUMMARY
#+BEGIN: block-update-summary
#+END:
** BURNDOWN CHART
#+BEGIN: block-update-graph
#+END:
** BURNDOWN LIST
#+PLOT: title:"Burndown" ind:1 deps:(3 4) set:"term dumb" set:"xtics scale 0.5" set:"ytics scale 0.5" file:"burndown.plt" set:"xrange [0:17]"
#+BEGIN: block-update-burndown
#+END:
** TASK LIST
#+BEGIN: columnview :hlines 2 :maxlevel 5 :id "TASKS"
#+END:
* TASKS
  :PROPERTIES:
  :ID:       TASKS
  :SPRINTLENGTH: 13
  :SPRINTSTART: <2021-10-07 Thu>
  :wpd-akshay196: 1
  :wpd-bhavin192: 1
  :wpd-gandalfdwite: 1
  :wpd-nihaal: 1
  :END:
** akshay196
*** DONE Read Kubernetes docs - Part V
    CLOSED: [2021-10-19 Tue 17:12]
    :PROPERTIES:
    :ESTIMATED: 8
    :ACTUAL:   4.82
    :OWNER:    akshay196
    :ID:       READ.1623432379
    :TASKID:   READ.1623432379
    :END:
    :LOGBOOK:
    CLOCK: [2021-10-19 Tue 16:27]--[2021-10-19 Tue 17:12] =>  0:45
    CLOCK: [2021-10-18 Mon 18:39]--[2021-10-18 Mon 19:40] =>  1:01
    CLOCK: [2021-10-17 Sun 11:19]--[2021-10-17 Sun 11:51] =>  0:32
    CLOCK: [2021-10-15 Fri 20:46]--[2021-10-15 Fri 21:48] =>  1:02
    CLOCK: [2021-10-13 Wed 18:01]--[2021-10-13 Wed 18:50] =>  0:49
    CLOCK: [2021-10-12 Tue 19:30]--[2021-10-12 Tue 20:10] =>  0:40
    :END:
     https://kubernetes.io/docs
     - [X] Volumes                                (4h)
     - [X] Persistent Volumes                     (4h)
*** DONE Let's Go Book - Part III
    CLOSED: [2021-10-11 Mon 19:09]
    :PROPERTIES:
    :ESTIMATED: 5
    :ACTUAL:   3.05
    :OWNER:    akshay196
    :ID:       READ.1629798238
    :TASKID:   READ.1629798238
    :END:
    :LOGBOOK:
    CLOCK: [2021-10-11 Mon 18:31]--[2021-10-11 Mon 19:09] =>  0:38
    CLOCK: [2021-10-09 Sat 19:35]--[2021-10-09 Sat 20:18] =>  0:43
    CLOCK: [2021-10-08 Fri 19:44]--[2021-10-08 Fri 20:47] =>  1:03
    CLOCK: [2021-10-07 Thu 21:02]--[2021-10-07 Thu 21:41] =>  0:39
    :END:
    - [X] 3.4. Centralized Error Handling                  (1h)
    - [X] 3.5. Isolating the Application Routes            (1h)
    - [X] 4.1. Setting Up MySQL                            (1h)
    - [X] 4.2. Installing a Database Driver                (1h)
    - [X] 4.3. Creating a Database Connection Pool         (1h)
** bhavin192
*** DONE Watch KubeCon EU 2021 - Part II [29/29]
    CLOSED: [2021-10-19 Tue 23:46]
    :PROPERTIES:
    :ESTIMATED: 13
    :ACTUAL:   10.98
    :OWNER:    bhavin192
    :ID:       READ.1629651751
    :TASKID:   READ.1629651751
    :END:
    :LOGBOOK:
    CLOCK: [2021-10-19 Tue 23:07]--[2021-10-19 Tue 23:46] =>  0:39
    CLOCK: [2021-10-19 Tue 21:52]--[2021-10-19 Tue 22:23] =>  0:31
    CLOCK: [2021-10-19 Tue 21:26]--[2021-10-19 Tue 21:52] =>  0:26
    CLOCK: [2021-10-18 Mon 23:23]--[2021-10-18 Mon 23:48] =>  0:25
    CLOCK: [2021-10-17 Sun 22:13]--[2021-10-17 Sun 23:09] =>  0:56
    CLOCK: [2021-10-17 Sun 21:53]--[2021-10-17 Sun 22:01] =>  0:08
    CLOCK: [2021-10-17 Sun 21:42]--[2021-10-17 Sun 21:49] =>  0:07
    CLOCK: [2021-10-17 Sun 20:21]--[2021-10-17 Sun 21:37] =>  1:16
    CLOCK: [2021-10-16 Sat 22:40]--[2021-10-16 Sat 23:28] =>  0:48
    CLOCK: [2021-10-16 Sat 20:13]--[2021-10-16 Sat 21:26] =>  1:13
    CLOCK: [2021-10-15 Fri 21:41]--[2021-10-15 Fri 22:19] =>  0:38
    CLOCK: [2021-10-15 Fri 20:37]--[2021-10-15 Fri 20:59] =>  0:22
    CLOCK: [2021-10-14 Thu 20:12]--[2021-10-14 Thu 21:13] =>  1:01
    CLOCK: [2021-10-13 Wed 23:52]--[2021-10-13 Wed 23:58] =>  0:06
    CLOCK: [2021-10-13 Wed 23:36]--[2021-10-13 Wed 23:44] =>  0:08
    CLOCK: [2021-10-12 Tue 20:57]--[2021-10-12 Tue 21:00] =>  0:03
    CLOCK: [2021-10-12 Tue 20:31]--[2021-10-12 Tue 20:56] =>  0:25
    CLOCK: [2021-10-11 Mon 20:52]--[2021-10-11 Mon 21:25] =>  0:33
    CLOCK: [2021-10-11 Mon 20:35]--[2021-10-11 Mon 20:52] =>  0:17
    CLOCK: [2021-10-10 Sun 22:22]--[2021-10-10 Sun 22:50] =>  0:28
    CLOCK: [2021-10-10 Sun 20:54]--[2021-10-10 Sun 21:23] =>  0:29
    :END:
    - [X] [[https://www.youtube.com/watch?v=WWKat7NP0NM][Multi-Tenancy in Kubernetes: How We Avoided Clusters Sprawl W... Dario Tranchitella & Maksim Fedotov - YouTube]]        (30m)
    - [X] [[https://www.youtube.com/watch?v=mcKNistZkrY][Akri: Making IoT Devices Accessible to Your Edge Kubernetes Clusters - Kate Goldenring & Jiří Appl - YouTube]]          (25m)
    - [X] [[https://www.youtube.com/watch?v=icyTnoonRqI][Automating Your Home with K3s and Home Assistant - Eddie Zaneski & Jeff Billimek - YouTube]]                            (30m)
    - [X] [[https://www.youtube.com/watch?v=HDdnZsWcSac][Taking the Helm: Becoming a Maintainer - Bridget Kromhout & Matt Butcher, Karena Angell, Matt Farina - YouTube]]        (35m)
    - [X] [[https://www.youtube.com/watch?v=ypq-nXh38Zk][Log Support in OpenTelemetry - Steve Flanders, Splunk - YouTube]]                                                       (30m)
    - [X] [[https://www.youtube.com/watch?v=dLe0TZEGhxo][Choose Wisely: Understanding Kubernetes Selectors - Christopher Hanson, RX-M,llc. - YouTube]]                           (35m)
    - [X] [[https://www.youtube.com/watch?v=A9IwOHGt7gM][CSI Volume Attacks – The SRE Strikes Back - Hendrik Land, NetApp - YouTube]]                                            (20m)
    - [X] [[https://www.youtube.com/watch?v=_Dzc07aBQHI][What Do You Mean K8s Doesn't Have Users? How Do I Manage User Access Then? - Jussi Nummelin - YouTube]]                 (30m)
    - [X] [[https://www.youtube.com/watch?v=fxqV24h_ocs][From Tweet to BadIdea: Creating an Embeddable Kubernetes Style API Server - Jason DeTiberus - YouTube]]                 (25m)
    - [X] [[https://www.youtube.com/watch?v=lnXeONAaQfs][Cloud Native Distributed Event Streaming from TiKV - Zixiong Liu, PingCAP - YouTube]]                                   (20m)
    - [X] [[https://www.youtube.com/watch?v=1OVpogSKxyI][Better Scalability & More Isolation? The Cortex “Shuffle Sharding” Story - Tom Wilkie, Grafana Labs - YouTube]]         (25m)
    - [X] [[https://www.youtube.com/watch?v=7qUDyQQ52Uk][How to Break your Kubernetes Cluster with Networking - Thomas Graf, Isovalent - YouTube]]                               (30m)
    - [X] [[https://www.youtube.com/watch?v=cABk-6Sdb20][eBPF on the Rise - Getting Started - Quentin Monnet, Isovalent - YouTube]]                                              (30m)
    - [X] [[https://www.youtube.com/watch?v=wEW2kVKxgss][Cert-Manager Beyond Ingress – Exploring the Variety of Use Cases - Matthew Bates, Jetstack - YouTube]]                  (30m)
    - [X] [[https://www.youtube.com/watch?v=parm7--PQIE][When Prometheus Can’t Take the Load Anymore - Liron Cohen, Riskified - YouTube]]                                        (30m)
    - [X] [[https://www.youtube.com/watch?v=Ti5Uj984CLY][The Art of Hiding Yourself - Lorenzo Fontana, Sysdig - YouTube]]                                                        (25m)
    - [X] [[https://www.youtube.com/watch?v=kv5zaJ_o3iQ]["Extend All The Things!": Cloud Provider Edition - Joe Betz, Google - YouTube]]                                         (35m)
    - [X] [[https://www.youtube.com/watch?v=PBIbQ4PBXBA][Contour, a High Performance Multitenant Ingress Controller for Kubernetes - Steve Sloka, VMware - YouTube]]             (30m)
    - [X] [[https://www.youtube.com/watch?v=Rx-ksmLUHEY][Isolate the Users! Supporting User Namespaces in K8s for Increased Security - Mauricio Vásquez - YouTube]]              (25m)
    - [X] [[https://www.youtube.com/watch?v=nqKYgeUtk8s][Minikube & Three Different Local Kubernetes Learning Environments - Anders Björklund & Predrag Rogic - YouTube]]        (35m)
    - [X] [[https://www.youtube.com/watch?v=YEtV9k25IUY][CNCF SIG-Runtime: The Cloud Native Runtimes Outlook - Ricardo Aravena & Renaud Gaubert - YouTube]]                      (25m)
    - [X] [[https://www.youtube.com/watch?v=dsuo1VQQZ2E][CNCF Serverless WG - Serverless Workflow Project Deep Dive - Tihomir Surdilovic & Doug Davis - YouTube]]                (30m)
    - [X] [[https://www.youtube.com/watch?v=VPBKNfRRytg][Cortex: Multi-tenant Scalable Prometheus - Bryan Boreham, Weaveworks & Jacob Tlisi, Grafana Labs - YouTube]]            (15m)
    - [X] [[https://www.youtube.com/watch?v=n1KiMD-Tids][Using a Distributed Key-Value Store - Nick Cameron & Andy Lok, PingCAP - YouTube]]                                      (25m)
    - [X] [[https://www.youtube.com/watch?v=vP7yLJUhtyE][The New Stack Makers Virtual Pancake Breakfast + Podcast: Securing GitOps: Self-Healing Through O... - YouTube]]        (30m)
    - [X] [[https://www.youtube.com/watch?v=pUDm8Xx4RXg][CNCF - SIG Network Intro and Deep Dive - Lee Calcote, Layer5 & Ken Owens, Fiserv - YouTube]]                            (25m)
    - [X] [[https://www.youtube.com/watch?v=EaIqBoiRivs][Graduated Project Lightning Talk: Using Trace Data for Monitoring & Alerting of Appl... Albert Teoh - YouTube]]         (10m)
    - [X] [[https://www.youtube.com/watch?v=IqrKFBxf_Dk][Battle of Black Friday: Proactively Autoscaling StockX - Mario Loria, Carta & Kyle Schrade, StockX - YouTube]]          (30m)
    - [X] [[https://www.youtube.com/watch?v=Ajc2ngGv0m8][Zero Pain Microservice Development and Deployment with Dapr and KubeVela - Hongchao Deng, Alibaba - YouTube]]           (15m)
** gandalfdwite
*** DONE CKS Video course - Part I [15/15]
    CLOSED: [2021-10-15 Fri 19:45]
    :PROPERTIES:
    :ESTIMATED: 11
    :ACTUAL:   11.08
    :OWNER: gandalfdwite
    :ID: OPS.1633617987
    :TASKID: OPS.1633617987
    :END:
    :LOGBOOK:
    CLOCK: [2021-10-14 Thu 21:55]--[2021-10-15 Fri 00:00] =>  2:05
    CLOCK: [2021-10-13 Wed 17:25]--[2021-10-13 Wed 19:10] =>  1:45
    CLOCK: [2021-10-12 Tue 23:10]--[2021-10-13 Wed 00:00] =>  0:50
    CLOCK: [2021-10-11 Mon 20:00]--[2021-10-11 Mon 21:00] =>  1:00
    CLOCK: [2021-10-10 Sun 13:15]--[2021-10-10 Sun 15:00] =>  1:45
    CLOCK: [2021-10-09 Sat 20:45]--[2021-10-09 Sat 21:55] =>  1:10
    CLOCK: [2021-10-08 Fri 21:00]--[2021-10-08 Fri 22:00] =>  1:00
    CLOCK: [2021-10-07 Thu 18:30]--[2021-10-07 Thu 20:00] =>  1:30
    :END:
    - [X] Section 1       ( 13 min )
    - [X] Section 2       ( 24 min )
    - [X] Section 3       ( 19 min )
    - [X] Section 4       ( 15 min )
    - [X] Section 5       ( 28 min )
    - [X] Section 6       ( 15 min )
    - [X] Section 7       ( 22 min )
    - [X] Section 8       ( 10 min )
    - [X] Section 9       ( 13 min )
    - [X] Section 10      ( 10 min )
    - [X] Section 11      ( 33 min )
    - [X] Section 12      ( 13 min )
    - [X] Section 13      ( 29 min )
    - [X] Section 14      ( 16 min )
    - [X] Section 15      ( 45 min )
*** DONE Work on Orko [2/2]
    CLOSED: [2021-10-20 Wed 08:26]
    :PROPERTIES:
    :Estimated: 8
    :ACTUAL:   9.17
    :OWNER: gandalfdwite
    :ID: DEV.1633618576
    :TASKID: DEV.1633618576
    :END:
    :LOGBOOK:
    CLOCK: [2021-10-19 Tue 22:00]--[2021-10-20 Wed 00:40] =>  2:40
    CLOCK: [2021-10-18 Mon 20:30]--[2021-10-18 Mon 22:30] =>  2:00
    CLOCK: [2021-10-16 Sat 11:30]--[2021-10-16 Sat 13:50] =>  2:20
    CLOCK: [2021-10-15 Fri 21:00]--[2021-10-15 Fri 23:10] =>  2:10
    :END:
    - [X] Improve current CLI functionality  ( 4h )
    - [X] Make changes in sqlite storage     ( 4h )

** nihaal
*** DONE Write blog post on eudyptula challenge task 5
    CLOSED: [2021-10-12 Tue 21:57]
    :PROPERTIES:
    :ESTIMATED: 4
    :ACTUAL:   5.82
    :OWNER:    nihaal
    :ID:       WRITE.1633495197
    :TASKID:   WRITE.1633495197
    :END:
    :LOGBOOK:
    CLOCK: [2021-10-12 Tue 18:16]--[2021-10-12 Tue 21:50] =>  3:34
    CLOCK: [2021-10-08 Fri 19:10]--[2021-10-08 Fri 21:25] =>  2:15
    :END:
    Published [[https://nihaal.me/post/ec5/][Here]]
*** DONE Read Linux Device Drivers, 3rd edition
    CLOSED: [2021-10-13 Wed 20:16]
    :PROPERTIES:
    :ESTIMATED: 3
    :ACTUAL:   3.03
    :OWNER: nihaal
    :ID: READ.1632069861
    :TASKID: READ.1632069861
    :END:
    :LOGBOOK:
    CLOCK: [2021-10-13 Wed 18:32]--[2021-10-13 Wed 20:15] =>  1:43
    CLOCK: [2021-10-11 Mon 18:33]--[2021-10-11 Mon 19:52] =>  1:19
    :END:
    - [X] 3. Char Drivers
*** DONE Eudyptula Challenge
    CLOSED: [2021-10-19 Tue 14:22]
    :PROPERTIES:
    :ESTIMATED: 5
    :ACTUAL:   5.07
    :OWNER: nihaal
    :ID: DEV.1632240155
    :TASKID: DEV.1632240155
    :END:
    :LOGBOOK:
    CLOCK: [2021-10-19 Tue 13:39]--[2021-10-19 Tue 14:22] =>  0:43
    CLOCK: [2021-10-18 Mon 13:48]--[2021-10-18 Mon 15:30] =>  1:42
    CLOCK: [2021-10-17 Sun 18:43]--[2021-10-17 Sun 20:35] =>  1:52
    CLOCK: [2021-10-16 Sat 18:32]--[2021-10-16 Sat 19:19] =>  0:47
    :END:
    - [X] 6. Misc Character driver
    - [X] 8. Creating debugfs entries
*** DONE Read Linux Weekly News
    CLOSED: [2021-10-14 Thu 19:27]
    :PROPERTIES:
    :ESTIMATED: 1.5
    :ACTUAL:   2.10
    :OWNER: nihaal
    :ID: READ.1632238538
    :TASKID: READ.1632238538
    :END:
    :LOGBOOK:
    CLOCK: [2021-10-14 Thu 18:25]--[2021-10-14 Thu 19:27] =>  1:02
    CLOCK: [2021-10-07 Thu 20:34]--[2021-10-07 Thu 21:09] =>  0:35
    CLOCK: [2021-10-07 Thu 19:44]--[2021-10-07 Thu 19:57] =>  0:13
    CLOCK: [2021-10-07 Thu 19:17]--[2021-10-07 Thu 19:33] =>  0:16
    :END:
    - [X] Weekly edition for September 30, 2021
    - [X] Weekly edition for October 7, 2021
   
