#+TITLE: December 5-17, 2021 (13 days)
#+PROPERTY: Effort_ALL 0 0:05 0:10 0:30 1:00 2:00 3:00 4:00
#+COLUMNS: %35ITEM %TASKID %OWNER %3PRIORITY %TODO %5ESTIMATED{+} %3ACTUAL{+}
* REPORTS
** SCRUM BOARD
#+BEGIN: block-update-board
#+END:
** DEVELOPER SUMMARY
#+BEGIN: block-update-summary
#+END:
** BURNDOWN CHART
#+BEGIN: block-update-graph
#+END:
** BURNDOWN LIST
#+PLOT: title:"Burndown" ind:1 deps:(3 4) set:"term dumb" set:"xtics scale 0.5" set:"ytics scale 0.5" file:"burndown.plt" set:"xrange [0:17]"
#+BEGIN: block-update-burndown
#+END:
** TASK LIST
#+BEGIN: columnview :hlines 2 :maxlevel 5 :id "TASKS"
#+END:
* TASKS
  :PROPERTIES:
  :ID:       TASKS
  :SPRINTLENGTH: 13
  :SPRINTSTART: <2021-12-05 Sun>
  :wpd-nihaal: 1
  :END:
** nihaal
*** DONE Eudyptula Challenge - Part IV
    :PROPERTIES:
    :ESTIMATED: 8
    :ACTUAL:   7.00
    :OWNER: nihaal
    :ID: DEV.1632240155
    :TASKID: DEV.1632240155
    :END:
    :LOGBOOK:
    CLOCK: [2021-12-17 Fri 18:24]--[2021-12-17 Fri 19:16] =>  0:52
    CLOCK: [2021-12-17 Fri 17:03]--[2021-12-17 Fri 17:50] =>  0:47
    CLOCK: [2021-12-15 Wed 18:59]--[2021-12-15 Wed 19:53] =>  0:54
    CLOCK: [2021-12-13 Mon 19:02]--[2021-12-13 Mon 20:13] =>  1:11
    CLOCK: [2021-12-08 Wed 21:33]--[2021-12-08 Wed 21:57] =>  0:24
    CLOCK: [2021-12-08 Wed 18:23]--[2021-12-08 Wed 20:01] =>  1:38
    CLOCK: [2021-12-06 Mon 18:42]--[2021-12-06 Mon 19:56] =>  1:14
    :END:
    - [X] 12. Working with Linked Lists               (2h)
    - [X] 13. Slab cache                              (2h)
    - [X] 14. Creating procfs entries                 (2h)
    - [X] 15. Create a new system call                (2h)
*** DONE Set up Qemu for kernel debugging
    :PROPERTIES:
    :ESTIMATED: 1
    :ACTUAL:   1.20
    :OWNER: nihaal
    :ID: OPS.1638711646
    :TASKID: OPS.1638711646
    :END:
    :LOGBOOK:
    CLOCK: [2021-12-12 Sun 19:09]--[2021-12-12 Sun 20:21] =>  1:12
    :END:
    - [X] Read https://www.collabora.com/news-and-blog/blog/2017/01/16/setting-up-qemu-kvm-for-kernel-development/
    - [X] Checkout [[https://github.com/amluto/virtme][virtme]]
      - Read https://www.collabora.com/news-and-blog/blog/2018/09/18/virtme-the-kernel-developers-best-friend/
*** DONE Read Linux Weekly News
    :PROPERTIES:
    :ESTIMATED: 2
    :ACTUAL:   2.00
    :OWNER: nihaal
    :ID: READ.1638710694
    :TASKID: READ.1638710694
    :END:
    :LOGBOOK:
    CLOCK: [2021-12-16 Thu 18:41]--[2021-12-16 Thu 19:49] =>  1:08
    CLOCK: [2021-12-09 Thu 20:45]--[2021-12-09 Thu 21:37] =>  0:52
    :END:
    - [X] Weekly edition for December 02, 2021        (1h)
    - [X] Weekly edition for December 09, 2021        (1h)
   
