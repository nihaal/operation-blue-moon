#+TITLE: December 18, 2021 - January 3, 2022 (17 days)
#+PROPERTY: Effort_ALL 0 0:05 0:10 0:30 1:00 2:00 3:00 4:00
#+COLUMNS: %35ITEM %TASKID %OWNER %3PRIORITY %TODO %5ESTIMATED{+} %3ACTUAL{+}
* REPORTS
** SCRUM BOARD
#+BEGIN: block-update-board
#+END:
** DEVELOPER SUMMARY
#+BEGIN: block-update-summary
#+END:
** BURNDOWN CHART
#+BEGIN: block-update-graph
#+END:
** BURNDOWN LIST
#+PLOT: title:"Burndown" ind:1 deps:(3 4) set:"term dumb" set:"xtics scale 0.5" set:"ytics scale 0.5" file:"burndown.plt" set:"xrange [0:17]"
#+BEGIN: block-update-burndown
#+END:
** TASK LIST
#+BEGIN: columnview :hlines 2 :maxlevel 5 :id "TASKS"
#+END:
* TASKS
  :PROPERTIES:
  :ID:       TASKS
  :SPRINTLENGTH: 17
  :SPRINTSTART: <2021-12-18 Sat>
  :wpd-nihaal: 1
  :END:
** nihaal
*** DONE Eudyptula Challenge - Part V
    :PROPERTIES:
    :ESTIMATED: 4
    :ACTUAL:   3.37
    :OWNER: nihaal
    :ID: DEV.1632240155
    :TASKID: DEV.1632240155
    :END:
    :LOGBOOK:
    CLOCK: [2022-01-03 Mon 18:27]--[2022-01-03 Mon 18:35] =>  0:08
    CLOCK: [2022-01-02 Sun 23:06]--[2022-01-02 Sun 23:28] =>  0:22
    CLOCK: [2022-01-02 Sun 20:53]--[2022-01-02 Sun 22:39] =>  1:46
    CLOCK: [2022-01-02 Sun 20:20]--[2022-01-02 Sun 20:26] =>  0:06
    CLOCK: [2022-01-02 Sun 18:51]--[2022-01-02 Sun 19:51] =>  1:00
    :END:
    - [X] 17. Create a kernel thread                  (2h)
    - [X] 18. Working with queues                     (2h)
*** Read Linux Device Drivers, 3rd edition - Part IV
    :PROPERTIES:
    :ESTIMATED: 8
    :ACTUAL:   6.72
    :OWNER: nihaal
    :ID: READ.1632069861
    :TASKID: READ.1632069861
    :END:
    :LOGBOOK:
    CLOCK: [2022-01-03 Mon 18:46]--[2022-01-03 Mon 19:51] =>  1:05
    CLOCK: [2021-12-31 Fri 20:33]--[2021-12-31 Fri 20:45] =>  0:12
    CLOCK: [2021-12-31 Fri 18:43]--[2021-12-31 Fri 20:03] =>  1:20
    CLOCK: [2021-12-28 Tue 18:44]--[2021-12-28 Tue 19:59] =>  1:15
    CLOCK: [2021-12-27 Mon 19:18]--[2021-12-27 Mon 20:01] =>  0:43
    CLOCK: [2021-12-22 Wed 18:51]--[2021-12-22 Wed 19:58] =>  1:07
    CLOCK: [2021-12-21 Tue 18:52]--[2021-12-21 Tue 19:53] =>  1:01
    :END:
    - [X] 6. Advanced Char Driver Operations          (2h)
    - [ ] 7. Time, Delays, and Deferred Work          (3h)
    - [ ] 8. Allocating Memory                        (3h)
*** DONE Read Linux Weekly News
    :PROPERTIES:
    :ESTIMATED: 2
    :ACTUAL:   1.3
    :OWNER: nihaal
    :ID: READ.1639822848
    :TASKID: READ.1639822848
    :END:
    :LOGBOOK:
    CLOCK: [2021-12-23 Thu 20:38]--[2021-12-23 Thu 21:18] =>  0:40
    CLOCK: [2021-12-23 Thu 19:17]--[2021-12-23 Thu 19:55] =>  0:38
    :END:
    - [X] Weekly edition for December 16, 2021        (1h)
    - +[ ] Weekly edition for December 23, 2021+        (1h)
