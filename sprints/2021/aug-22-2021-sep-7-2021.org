#+TITLE: August 22, 2021 - September 7, 2021 (17 days)
#+PROPERTY: Effort_ALL 0 0:05 0:10 0:30 1:00 2:00 3:00 4:00
#+COLUMNS: %35ITEM %TASKID %OWNER %3PRIORITY %TODO %5ESTIMATED{+} %3ACTUAL{+}
* REPORTS
** SCRUM BOARD
#+BEGIN: block-update-board
#+END:
** DEVELOPER SUMMARY
#+BEGIN: block-update-summary
#+END:
** BURNDOWN CHART
#+BEGIN: block-update-graph
#+END:
** BURNDOWN LIST
#+PLOT: title:"Burndown" ind:1 deps:(3 4) set:"term dumb" set:"xtics scale 0.5" set:"ytics scale 0.5" file:"burndown.plt" set:"xrange [0:17]"
#+BEGIN: block-update-burndown
#+END:
** TASK LIST
#+BEGIN: columnview :hlines 2 :maxlevel 5 :id "TASKS"
#+END:
* TASKS
  :PROPERTIES:
  :ID:       TASKS
  :SPRINTLENGTH: 17
  :SPRINTSTART: <2021-08-22 Sun>
  :wpd-akshay196: 1
  :wpd-bhavin192: 1
  :END:
** akshay196
*** DONE Blog post on gRPC - Part I
    CLOSED: [2021-09-04 Sat 11:17]
    :PROPERTIES:
    :ESTIMATED: 4
    :ACTUAL:   1.37
    :OWNER: akshay196
    :ID: WRITE.1628415262
    :TASKID: WRITE.1628415262
    :END:
    :LOGBOOK:
    CLOCK: [2021-09-04 Sat 09:55]--[2021-09-04 Sat 11:17] =>  1:22
    :END:
*** DONE PyCon India Mentorship WG Volunteering
    CLOSED: [2021-08-27 Fri 22:15]
    :PROPERTIES:
    :ESTIMATED: 8
    :ACTUAL:   6.33
    :OWNER: akshay196
    :ID: TASK.1631338791
    :TASKID: TASK.1631338791
    :END:
    :LOGBOOK:
    CLOCK: [2021-08-27 Fri 21:30]--[2021-08-27 Fri 22:15] =>  0:45
    CLOCK: [2021-08-26 Thu 15:35]--[2021-08-26 Thu 16:50] =>  1:15
    CLOCK: [2021-08-25 Wed 18:10]--[2021-08-25 Wed 19:20] =>  1:10
    CLOCK: [2021-08-24 Tue 20:50]--[2021-08-24 Tue 21:35] =>  0:45
    CLOCK: [2021-08-23 Mon 19:15]--[2021-08-23 Mon 20:05] =>  0:50
    CLOCK: [2021-08-22 Sun 20:05]--[2021-08-22 Sun 21:40] =>  1:35
    :END:
*** DONE Let's Go Book - Part I
    CLOSED: [2021-08-28 Sat 11:55]
    :PROPERTIES:
    :ESTIMATED: 1
    :ACTUAL:   1.45
    :OWNER: akshay196
    :ID: READ.1629798238
    :TASKID: READ.1629798238
    :END:
    :LOGBOOK:
    CLOCK: [2021-08-28 Sat 10:28]--[2021-08-28 Sat 11:55] =>  1:27
    :END:
    - [X] Chapter 3.1. Managing Configuration Settings    (1h)
** bhavin192
*** DONE Help PyCon India mentorship workgroup
    CLOSED: [2021-09-07 Tue 23:29]
    :PROPERTIES:
    :ESTIMATED: 17
    :ACTUAL:   18.93
    :OWNER:    bhavin192
    :ID:       EVENT.1631464997
    :TASKID:   EVENT.1631464997
    :END:
    :LOGBOOK:
    CLOCK: [2021-09-07 Tue 22:10]--[2021-09-07 Tue 23:29] =>  1:19
    CLOCK: [2021-09-06 Mon 18:33]--[2021-09-06 Mon 19:33] =>  1:00
    CLOCK: [2021-09-05 Sun 21:35]--[2021-09-05 Sun 22:40] =>  1:05
    CLOCK: [2021-09-04 Sat 21:01]--[2021-09-04 Sat 22:03] =>  1:02
    CLOCK: [2021-09-03 Fri 18:01]--[2021-09-03 Fri 19:02] =>  1:01
    CLOCK: [2021-09-02 Thu 17:35]--[2021-09-02 Thu 18:32] =>  0:57
    CLOCK: [2021-09-01 Wed 19:30]--[2021-09-01 Wed 20:50] =>  1:20
    CLOCK: [2021-08-31 Tue 22:45]--[2021-08-31 Tue 23:48] =>  1:03
    CLOCK: [2021-08-30 Mon 10:50]--[2021-08-30 Mon 11:03] =>  0:13
    CLOCK: [2021-08-29 Sun 23:40]--[2021-08-30 Mon 00:17] =>  0:37
    CLOCK: [2021-08-29 Sun 21:03]--[2021-08-29 Sun 22:41] =>  1:38
    CLOCK: [2021-08-28 Sat 15:30]--[2021-08-28 Sat 16:44] =>  1:14
    CLOCK: [2021-08-27 Fri 18:01]--[2021-08-27 Fri 18:44] =>  0:43
    CLOCK: [2021-08-27 Fri 12:09]--[2021-08-27 Fri 12:17] =>  0:08
    CLOCK: [2021-08-26 Thu 13:29]--[2021-08-26 Thu 14:30] =>  1:01
    CLOCK: [2021-08-25 Wed 21:48]--[2021-08-25 Wed 22:03] =>  0:15
    CLOCK: [2021-08-25 Wed 19:30]--[2021-08-25 Wed 20:14] =>  0:44
    CLOCK: [2021-08-24 Tue 19:50]--[2021-08-24 Tue 21:10] =>  1:20
    CLOCK: [2021-08-23 Mon 22:15]--[2021-08-23 Mon 23:30] =>  1:15
    CLOCK: [2021-08-22 Sun 22:48]--[2021-08-22 Sun 23:49] =>  1:01
    :END:
