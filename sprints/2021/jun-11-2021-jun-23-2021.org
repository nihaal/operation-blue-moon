#+TITLE: June 11-23, 2021 (13 days)
#+PROPERTY: Effort_ALL 0 0:05 0:10 0:30 1:00 2:00 3:00 4:00
#+COLUMNS: %35ITEM %TASKID %OWNER %3PRIORITY %TODO %5ESTIMATED{+} %3ACTUAL{+}
* REPORTS
** SCRUM BOARD
#+BEGIN: block-update-board
#+END:
** DEVELOPER SUMMARY
#+BEGIN: block-update-summary
#+END:
** BURNDOWN CHART
#+BEGIN: block-update-graph
#+END:
** BURNDOWN LIST
#+PLOT: title:"Burndown" ind:1 deps:(3 4) set:"term dumb" set:"xtics scale 0.5" set:"ytics scale 0.5" file:"burndown.plt" set:"xrange [0:17]"
#+BEGIN: block-update-burndown
#+END:
** TASK LIST
#+BEGIN: columnview :hlines 2 :maxlevel 5 :id "TASKS"
#+END:
* TASKS
  :PROPERTIES:
  :ID:       TASKS
  :SPRINTLENGTH: 13
  :SPRINTSTART: <2021-06-11 Fri>
  :wpd-bhavin192: 1
  :END:
** bhavin192
*** DONE Production Kubernetes - 3. Container Runtime
    CLOSED: [2021-06-13 Sun 16:49]
    :PROPERTIES:
    :ESTIMATED: 3
    :ACTUAL:   2.50
    :OWNER:    bhavin192
    :ID:       READ.1622393322
    :TASKID:   READ.1622393322
    :END:
    :LOGBOOK:
    CLOCK: [2021-06-13 Sun 16:26]--[2021-06-13 Sun 16:49] =>  0:23
    CLOCK: [2021-06-13 Sun 15:53]--[2021-06-13 Sun 16:21] =>  0:28
    CLOCK: [2021-06-12 Sat 17:16]--[2021-06-12 Sat 18:21] =>  1:05
    CLOCK: [2021-06-11 Fri 20:11]--[2021-06-11 Fri 20:45] =>  0:34
    :END:
    https://learning.oreilly.com/library/view/production-kubernetes/9781492092292/
*** DONE Production Kubernetes - 4. Container Storage
    CLOSED: [2021-06-19 Sat 18:02]
    :PROPERTIES:
    :ESTIMATED: 5
    :ACTUAL:   6.08
    :OWNER:    bhavin192
    :ID:       READ.1622393322
    :TASKID:   READ.1622393322
    :END:
    :LOGBOOK:
    CLOCK: [2021-06-19 Sat 17:40]--[2021-06-19 Sat 18:02] =>  0:22
    CLOCK: [2021-06-19 Sat 17:08]--[2021-06-19 Sat 17:13] =>  0:05
    CLOCK: [2021-06-19 Sat 16:05]--[2021-06-19 Sat 17:00] =>  0:55
    CLOCK: [2021-06-18 Fri 20:31]--[2021-06-18 Fri 21:01] =>  0:30
    CLOCK: [2021-06-17 Thu 19:45]--[2021-06-17 Thu 20:47] =>  1:02
    CLOCK: [2021-06-16 Wed 20:24]--[2021-06-16 Wed 21:03] =>  0:39
    CLOCK: [2021-06-16 Wed 19:48]--[2021-06-16 Wed 20:19] =>  0:31
    CLOCK: [2021-06-15 Tue 19:45]--[2021-06-15 Tue 20:45] =>  1:00
    CLOCK: [2021-06-14 Mon 19:43]--[2021-06-14 Mon 20:44] =>  1:01
    :END:
    https://learning.oreilly.com/library/view/production-kubernetes/9781492092292/
*** DONE Production Kubernetes - 5. Pod Networking - Part I
    CLOSED: [2021-06-23 Wed 20:32]
    :PROPERTIES:
    :ESTIMATED: 5
    :ACTUAL:   3.35
    :OWNER:    bhavin192
    :ID:       READ.1622393322
    :TASKID:   READ.1622393322
    :END:
    :LOGBOOK:
    CLOCK: [2021-06-23 Wed 19:47]--[2021-06-23 Wed 20:32] =>  0:45
    CLOCK: [2021-06-22 Tue 21:00]--[2021-06-22 Tue 21:13] =>  0:13
    CLOCK: [2021-06-22 Tue 20:17]--[2021-06-22 Tue 20:50] =>  0:33
    CLOCK: [2021-06-21 Mon 20:08]--[2021-06-21 Mon 21:05] =>  0:57
    CLOCK: [2021-06-21 Mon 19:51]--[2021-06-21 Mon 19:54] =>  0:03
    CLOCK: [2021-06-20 Sun 16:11]--[2021-06-20 Sun 17:01] =>  0:50
    :END:
    https://learning.oreilly.com/library/view/production-kubernetes/9781492092292/

