#+TITLE: September 20-October 6, 2021 (17 days)
#+PROPERTY: Effort_ALL 0 0:05 0:10 0:30 1:00 2:00 3:00 4:00
#+COLUMNS: %35ITEM %TASKID %OWNER %3PRIORITY %TODO %5ESTIMATED{+} %3ACTUAL{+}
* REPORTS
** SCRUM BOARD
#+BEGIN: block-update-board
#+END:
** DEVELOPER SUMMARY
#+BEGIN: block-update-summary
#+END:
** BURNDOWN CHART
#+BEGIN: block-update-graph
#+END:
** BURNDOWN LIST
#+PLOT: title:"Burndown" ind:1 deps:(3 4) set:"term dumb" set:"xtics scale 0.5" set:"ytics scale 0.5" file:"burndown.plt" set:"xrange [0:17]"
#+BEGIN: block-update-burndown
#+END:
** TASK LIST
#+BEGIN: columnview :hlines 2 :maxlevel 5 :id "TASKS"
#+END:
* TASKS
  :PROPERTIES:
  :ID:       TASKS
  :SPRINTLENGTH: 17
  :SPRINTSTART: <2021-09-20 Mon>
  :wpd-bhavin192: 1
  :wpd-gandalfdwite: 1
  :END:
** bhavin192
*** DONE Help with PyCon India 2021 video processing
    CLOSED: [2021-09-26 Sun 17:51]
    :PROPERTIES:
    :ESTIMATED: 4
    :ACTUAL:   4.90
    :OWNER:    bhavin192
    :ID:       EVENT.1633541465
    :TASKID:   EVENT.1633541465
    :END:
    :LOGBOOK:
    CLOCK: [2021-09-26 Sun 16:27]--[2021-09-26 Sun 17:51] =>  1:24
    CLOCK: [2021-09-26 Sun 15:31]--[2021-09-26 Sun 16:23] =>  0:52
    CLOCK: [2021-09-26 Sun 13:54]--[2021-09-26 Sun 15:02] =>  1:08
    CLOCK: [2021-09-25 Sat 22:28]--[2021-09-25 Sat 23:58] =>  1:30
    :END:
*** DONE Watch KubeCon EU 2021 - Part I [29/29]
    CLOSED: [2021-10-06 Wed 23:41]
    :PROPERTIES:
    :ESTIMATED: 13
    :ACTUAL:   11.70
    :OWNER:    bhavin192
    :ID:       READ.1629651751
    :TASKID:   READ.1629651751
    :END:
    :LOGBOOK:
    CLOCK: [2021-10-06 Wed 22:14]--[2021-10-06 Wed 23:41] =>  1:27
    CLOCK: [2021-10-05 Tue 22:04]--[2021-10-05 Tue 22:49] =>  0:45
    CLOCK: [2021-10-05 Tue 21:08]--[2021-10-05 Tue 21:25] =>  0:17
    CLOCK: [2021-10-04 Mon 21:46]--[2021-10-04 Mon 22:38] =>  0:52
    CLOCK: [2021-10-04 Mon 21:10]--[2021-10-04 Mon 21:14] =>  0:04
    CLOCK: [2021-10-04 Mon 20:40]--[2021-10-04 Mon 21:05] =>  0:25
    CLOCK: [2021-10-03 Sun 22:05]--[2021-10-03 Sun 22:31] =>  0:26
    CLOCK: [2021-10-03 Sun 21:37]--[2021-10-03 Sun 21:39] =>  0:02
    CLOCK: [2021-10-03 Sun 20:28]--[2021-10-03 Sun 21:10] =>  0:42
    CLOCK: [2021-10-03 Sun 18:09]--[2021-10-03 Sun 19:50] =>  1:41
    CLOCK: [2021-10-02 Sat 23:14]--[2021-10-02 Sat 23:44] =>  0:30
    CLOCK: [2021-09-30 Thu 22:08]--[2021-09-30 Thu 22:15] =>  0:07
    CLOCK: [2021-09-30 Thu 20:48]--[2021-09-30 Thu 21:26] =>  0:38
    CLOCK: [2021-09-29 Wed 22:09]--[2021-09-29 Wed 22:35] =>  0:26
    CLOCK: [2021-09-29 Wed 20:51]--[2021-09-29 Wed 21:17] =>  0:26
    CLOCK: [2021-09-28 Tue 22:46]--[2021-09-28 Tue 22:58] =>  0:12
    CLOCK: [2021-09-28 Tue 20:42]--[2021-09-28 Tue 21:25] =>  0:43
    CLOCK: [2021-09-27 Mon 22:24]--[2021-09-27 Mon 22:28] =>  0:04
    CLOCK: [2021-09-27 Mon 22:01]--[2021-09-27 Mon 22:20] =>  0:19
    CLOCK: [2021-09-27 Mon 20:41]--[2021-09-27 Mon 21:18] =>  0:37
    CLOCK: [2021-09-25 Sat 19:19]--[2021-09-25 Sat 19:27] =>  0:08
    CLOCK: [2021-09-23 Thu 21:26]--[2021-09-23 Thu 22:09] =>  0:43
    CLOCK: [2021-09-23 Thu 20:40]--[2021-09-23 Thu 20:48] =>  0:08
    :END:
    - [X] [[https://www.youtube.com/watch?v=y4UF9ML0BDw][K8s Labels Everywhere! Decluttering With Node Profile Discovery. - Conor Nolan & Dave Cremins, Intel - YouTube]]        (30m)
    - [X] [[https://www.youtube.com/watch?v=OuKm2SiwnGM][Petabyte Scale Logging with Fluentd and Fluent Bit: A Use Case From... Hanzel Jesheen & Anurag Gupta - YouTube]]        (25m)
    - [X] [[https://www.youtube.com/watch?v=i6MQb8QsUdU][COSI: The Common Operating System Interface - Steven Borrelli & Andrew Rynhard - YouTube]]                              (25m)
    - [X] [[https://www.youtube.com/watch?v=UyO5C8qHUXw][Tackling New Challenges in a Virtual Focused Community - Bob Killen & Alison Dowdney - YouTube]]                        (25m)
    - [X] [[https://www.youtube.com/watch?v=BwQmjunIsFI][Maximizing Workload's Performance With Smarter Runtimes - Krisztian Litkey & Alexander Kanevskiy - YouTube]]            (30m)
    - [X] [[https://www.youtube.com/watch?v=o5h6s3A9bXY][The Long, Winding and Bumpy Road to CronJob’s GA - Maciej Szulik, Red Hat & Alay Patel, Red Hat - YouTube]]             (35m)
    - [X] [[https://www.youtube.com/watch?v=cGJXkZ7jiDk][xDS in gRPC for Service Mesh - Megan Yahya, Google LLC - YouTube]]                                                      (20m)
    - [X] [[https://www.youtube.com/watch?v=h9QYbaJzBe0][Towards CNI v2.0 - Casey Callendrello, Red Hat - YouTube]]                                                              (30m)
    - [X] [[https://www.youtube.com/watch?v=Nn-qrp0TRnM][SIG-network: Updates and Future Directions - Bowei Du & Tim Hockin, Google - YouTube]]                                  (20m)
    - [X] [[https://www.youtube.com/watch?v=F7B_TBcIyl8][Flux: Multi-tenancy Deep Dive - Philip Laine, Xenit - YouTube]]                                                         (25m)
    - [X] [[https://www.youtube.com/watch?v=hCTgCRlU-M0][Helm Users! What Flux 2 Can Do For You - Scott Rigby & Kingdon Barrett, Weaveworks - YouTube]]                          (35m)
    - [X] [[https://www.youtube.com/watch?v=v-Tq9V4zpPA][Achieving the Tipping Point for Open-source Software: Making the Business Value Obvi... Joshua Grose - YouTube]]        (25m)
    - [X] [[https://www.youtube.com/watch?v=43g8uPohTgc][Groupless Autoscaling with Karpenter - Ellis Tarn & Prateek Gogia, Amazon - YouTube]]                                   (35m)
    - [X] [[https://www.youtube.com/watch?v=LNMPg5n2lf0][Introduction and Deep Dive Into Containerd - Kohei Tokunaga & Akihiro Suda, NTT Corporation - YouTube]]                 (30m)
    - [X] [[https://www.youtube.com/watch?v=74VpVe-pZx0][App Delivery in Cloud Native: Where are We? What's Next? - Lei Zhang, Alibaba & Alois Reitbauer - YouTube]]             (30m)
    - [X] [[https://www.youtube.com/watch?v=odxPyW_rZNQ][SIG-Autoscaling Deep Dive and Q+A - Maciek Pytel & Marcin Wielgus, Google - YouTube]]                                   (20m)
    - [X] [[https://www.youtube.com/watch?v=nx1ABG8-uvs][SIG Multicluster Intro - Paul Morie, Red Hat & Jeremy Olmsted-Thompson, Google - YouTube]]                              (20m)
    - [X] [[https://www.youtube.com/watch?v=eRLW-eQ3KxQ][Mental Health In Unprecedented Times - Dr. Jennifer Akullian, Growth Coaching Institute - YouTube]]                     (25m)
    - [X] [[https://www.youtube.com/watch?v=SZMbuirEQVU][Notary v2: Supply Chain Security for Containers - Justin Cormack, Docker & Steve Lasker, Microsoft - YouTube]]          (30m)
    - [X] [[https://www.youtube.com/watch?v=Kvog7aZ3esY][Dedicated Infrastructure in a Multitenant World - Carlos Sanchez, Adobe - YouTube]]                                     (30m)
    - [X] [[https://www.youtube.com/watch?v=g5tHHD4crtQ][Traces from Events: A New Way to Visualise Kubernetes Activities - Bryan Boreham, Weaveworks - YouTube]]                (25m)
    - [X] [[https://www.youtube.com/watch?v=-18U84TS75o][Kubernetes Advanced Networking Testing with KIND - Antonio Ojea, RedHat - YouTube]]                                     (30m)
    - [X] [[https://www.youtube.com/watch?v=6sfr2IGJQXk][How We are Dealing with Metrics at Scale on GitLab.com - Andrew Newdigate, GitLab - YouTube]]                           (20m)
    - [X] [[https://www.youtube.com/watch?v=w0k7MI6sCJg][Secrets Store CSI Driver: Keeping Secrets Secret - Anish Ramasekar, Microsoft & Tommy Murphy, Google - YouTube]]        (25m)
    - [X] [[https://www.youtube.com/watch?v=HgRJpV-jvGU][Discontiguous CIDRs for Dynamic Cluster Scaling - Rahul Joshi & Sudeep Modi, Google - YouTube]]                         (30m)
    - [X] [[https://www.youtube.com/watch?v=lCRuzWFJBO0][Gateway API: A New Set of Kubernetes APIs for Advanced Traffic Routing - Harry Bagdi & Rob Scott - YouTube]]            (35m)
    - [X] [[https://www.youtube.com/watch?v=ewCnqDgewMg][How DoD Uses K8s & Flux to Achieve Compliance & Deployment Consistency - M. Medellin & G. Tillman - YouTube]]           (25m)
    - [X] [[https://www.youtube.com/watch?v=iyskOvOKHVw][From Zero to Hero: Outcomes from Cloud-Native El Salvador Comm... Raul Flamenco & Mauricio Quevedo - YouTube]]          (20m)
    - [X] [[https://www.youtube.com/watch?v=YB5rlo2cq9s][Sidecars at Netflix: From Basic Injection to First Class Ci... Rodrigo Campos Catelin & Manas Alekar - YouTube]]        (25m)
** gandalfdwite
*** DONE Blog post on Project Calico [1/1]
    CLOSED: [2021-09-26 Sun 19:49]
    :PROPERTIES:
    :ESTIMATED: 6
    :ACTUAL:   6.00
    :OWNER: gandalfdwite
    :ID: WRITE.1632406120
    :TASKID: WRITE.1632406120
    :END:
    :LOGBOOK:
    CLOCK: [2021-09-26 Sun 18:40]--[2021-09-26 Sun 20:00] =>  1:20
    CLOCK: [2021-09-25 Sat 11:30]--[2021-09-25 Sat 13:00] =>  1:30
    CLOCK: [2021-09-22 Wed 17:00]--[2021-09-22 Wed 18:00] =>  1:00
    CLOCK: [2021-09-21 Tue 22:15]--[2021-09-21 Tue 23:00] =>  0:45
    CLOCK: [2021-09-20 Mon 19:35]--[2021-09-20 Mon 21:00] =>  1:25
    :END:
    - [X] Blog post on Calico    ( 6h )
*** DONE Work on Orko project [2/2]
    CLOSED: [2021-10-07 Thu 17:12]
    :PROPERTIES:
    :ESTIMATED: 11
    :ACTUAL:   11.42
    :OWNER: gandalfdwite
    :ID: DEV.1632406499
    :TASKID: DEV.1632406499
    :END:
    :LOGBOOK:
    CLOCK: [2021-10-06 Wed 20:30]--[2021-10-06 Wed 21:15] =>  0:45
    CLOCK: [2021-10-05 Tue 19:05]--[2021-10-05 Tue 21:15] =>  2:10
    CLOCK: [2021-10-04 Mon 21:30]--[2021-10-04 Mon 22:45] =>  1:15
    CLOCK: [2021-10-03 Sun 10:10]--[2021-10-03 Sun 12:20] =>  2:10
    CLOCK: [2021-10-02 Sat 13:30]--[2021-10-02 Sat 15:25] =>  1:55
    CLOCK: [2021-09-30 Thu 19:00]--[2021-09-30 Thu 20:00] =>  1:00
    CLOCK: [2021-09-29 Wed 07:30]--[2021-09-29 Wed 08:30] =>  1:00
    CLOCK: [2021-09-28 Tue 21:30]--[2021-09-28 Tue 22:40] =>  1:10
    :END:
    - [X] Build CLI for Orko ( 5h )
    - [X] Integration with Sqlite ( 6h )
