#+TITLE: July 24-August 9, 2021 (17 days)
#+PROPERTY: Effort_ALL 0 0:05 0:10 0:30 1:00 2:00 3:00 4:00
#+COLUMNS: %35ITEM %TASKID %OWNER %3PRIORITY %TODO %5ESTIMATED{+} %3ACTUAL{+}
* REPORTS
** SCRUM BOARD
#+BEGIN: block-update-board
#+END:
** DEVELOPER SUMMARY
#+BEGIN: block-update-summary
#+END:
** BURNDOWN CHART
#+BEGIN: block-update-graph
#+END:
** BURNDOWN LIST
#+PLOT: title:"Burndown" ind:1 deps:(3 4) set:"term dumb" set:"xtics scale 0.5" set:"ytics scale 0.5" file:"burndown.plt" set:"xrange [0:17]"
#+BEGIN: block-update-burndown
#+END:
** TASK LIST
#+BEGIN: columnview :hlines 2 :maxlevel 5 :id "TASKS"
#+END:
* TASKS
  :PROPERTIES:
  :ID:       TASKS
  :SPRINTLENGTH: 17
  :SPRINTSTART: <2021-07-24 Sat>
  :wpd-bhavin192: 1
  :wpd-gandalfdwite: 1
  :END:
** bhavin192
*** DONE Attend Nest with Fedora 2021
    CLOSED: [2021-08-09 Mon 20:53]
    :PROPERTIES:
    :ESTIMATED: 8
    :ACTUAL:   8.02
    :OWNER:    bhavin192
    :ID:       EVENT.1627222136
    :TASKID:   EVENT.1627222136
    :END:
    :LOGBOOK:
    CLOCK: [2021-08-09 Mon 20:06]--[2021-08-09 Mon 20:53] =>  0:47
    CLOCK: [2021-08-08 Sun 19:09]--[2021-08-08 Sun 20:41] =>  1:32
    CLOCK: [2021-08-07 Sat 23:10]--[2021-08-07 Sat 23:50] =>  0:40
    CLOCK: [2021-08-07 Sat 20:31]--[2021-08-07 Sat 20:59] =>  0:28
    CLOCK: [2021-08-07 Sat 18:03]--[2021-08-07 Sat 19:09] =>  1:06
    CLOCK: [2021-08-05 Thu 20:30]--[2021-08-05 Thu 22:03] =>  1:33
    CLOCK: [2021-08-05 Thu 19:30]--[2021-08-05 Thu 20:20] =>  0:50
    CLOCK: [2021-08-05 Thu 19:05]--[2021-08-05 Thu 19:12] =>  0:07
    CLOCK: [2021-08-05 Thu 17:55]--[2021-08-05 Thu 18:53] =>  0:58
    :END:
    https://hopin.com/events/nest-with-fedora-2021
    https://web.archive.org/web/20210725142730/https://flocktofedora.org/

** gandalfdwite
*** DONE Blog Post on Kubernetes etcd and KDD [1/1]
    CLOSED: [2021-08-10 Tue 16:07]
    :PROPERTIES:
    :ESTIMATED: 4
    :ACTUAL:   4.50
    :OWNER: gandalfdwite
    :ID: WRITE.1627308391
    :TASKID: WRITE.1627308391
    :END:
    CLOCK: [2021-08-09 Mon 21:00]--[2021-08-09 Mon 21:30] =>  0:30
    CLOCK: [2021-08-08 Sun 13:45]--[2021-08-08 Sun 15:30] =>  1:45
    CLOCK: [2021-08-07 Sat 09:00]--[2021-08-07 Sat 10:15] =>  1:15
    CLOCK: [2021-08-06 Fri 20:30]--[2021-08-06 Fri 21:30] =>  1:00
    - [X] Blog Post on Kubernetes etcd and KDD  ( 4h )

*** DONE Kubernetes Intermediate in 3 Weeks (OReilly Training) [3/3]
    CLOSED: [2021-08-05 Thu 19:54]
    :PROPERTIES:
    :ESTIMATED: 9
    :ACTUAL:   9.13
    :OWNER: gandalfdwite
    :ID: EVENT.1627308143
    :TASKID: EVENT.1627308143
    :END:
    :LOGBOOK:
    CLOCK: [2021-08-04 Wed 21:00]--[2021-08-04 Wed 23:00] =>  2:00
    CLOCK: [2021-08-03 Tue 22:35]--[2021-08-04 Wed 00:20] =>  1:45
    CLOCK: [2021-08-02 Mon 07:00]--[2021-08-02 Mon 08:00] =>  1:00
    CLOCK: [2021-08-01 Sun 14:05]--[2021-08-01 Sun 15:55] =>  1:50
    CLOCK: [2021-07-31 Sat 11:30]--[2021-07-31 Sat 12:45] =>  1:15
    CLOCK: [2021-07-30 Fri 18:15]--[2021-07-30 Fri 19:33] =>  1:18
    :END:
    - [X] Recordings for Week 1    ( 3h )
    - [X] Recordings for Week 2    ( 3h )
    - [X] Recordings for Week 3    ( 3h )

*** DONE Blog Post on project Calico [1/1]
    CLOSED: [2021-07-29 Thu 15:33]
    :PROPERTIES:
    :ESTIMATED: 4h
    :ACTUAL:   4.98
    :OWNER: gandalfdwite
    :ID: WRITE.1627307951
    :TASKID: WRITE.1627307951
    :END:
    :LOGBOOK:
    CLOCK: [2021-07-26 Mon 19:55]--[2021-07-26 Mon 21:05] =>  1:10
    CLOCK: [2021-07-25 Sun 10:30]--[2021-07-25 Sun 12:30] =>  2:00
    CLOCK: [2021-07-24 Sat 15:16]--[2021-07-24 Sat 17:05] =>  1:49
    :END:
    - [X] Blog post on project calico    ( 4h )
   
