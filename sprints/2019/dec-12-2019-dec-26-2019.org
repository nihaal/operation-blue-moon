#+TITLE: December 12-26, 2019 (15 days)
#+PROPERTY: Effort_ALL 0 0:05 0:10 0:30 1:00 2:00 3:00 4:00
#+COLUMNS: %35ITEM %TASKID %OWNER %3PRIORITY %TODO %5ESTIMATED{+} %3ACTUAL{+}
* REPORTS
** SCRUM BOARD
#+BEGIN: block-update-board
| TODO | NEXT | IN_PROGRESS | WAITING | DONE                                                          | CANCELED |
|------+------+-------------+---------+---------------------------------------------------------------+----------|
|      |      |             |         | READ.1576169089. The Pragmatic Programmer: your (2019-12-26)  |          |
|      |      |             |         | WRITE.1576502914. Write blog post about Ansible (2019-12-26)  |          |
|      |      |             |         | WRITE.1576074953. Write a blog post on TravisCI (2019-12-26)  |          |
|      |      |             |         | DEV.1576217466. Swift course(FASTAI course) (2019-12-26)      |          |
|      |      |             |         | WRITE.1576217766. Pytorch kaggle kernel & dlwpt  (2019-12-26) |          |
|      |      |             |         | READ.1576218020. Learn Emacs (2019-12-26)                     |          |
|      |      |             |         | READ.1576157663. OAuth Training (2019-12-26)                  |          |
|      |      |             |         | READ.1573385682. Fluent Python Part III (2019-12-24)          |          |
|      |      |             |         | READ.1573405076. Linux Under the Hood (2019-12-22)            |          |
|      |      |             |         | WRITE.1575286599. Write blog post about EmacsCon (2019-12-19) |          |
#+END:
** DEVELOPER SUMMARY
#+BEGIN: block-update-summary
| NAME         | ESTIMATED | ACTUAL | DONE | REMAINING | PENCILS DOWN | PROGRESS   |
|--------------+-----------+--------+------+-----------+--------------+------------|
| Sandeepk     |        13 |   8.55 |   13 |         0 |   2020-01-09 | ########## |
| Kurianbenoy  |        16 | 16.946 |   16 |         0 |   2020-01-09 | ########## |
| Gandalfdwite |        15 |  15.74 |   15 |         0 |   2020-01-09 | ########## |
| Bhavin192    |      12.5 |  13.88 | 12.5 |         0 |   2020-01-09 | ########## |
| Akshay196    |        15 |  14.13 |   15 |         0 |   2020-01-09 | ########## |
#+END:
** BURNDOWN CHART
#+BEGIN: block-update-graph

#+END:
** BURNDOWN LIST
#+PLOT: title:"Burndown" ind:1 deps:(3 4) set:"term dumb" set:"xtics scale 0.5" set:"ytics scale 0.5" file:"burndown.plt" set:"xrange [0:15]"
#+BEGIN: block-update-burndown
| DAY |       DATE | ACTUAL | IDEAL | TASKS COMPLETED                                                                                                   |
|-----+------------+--------+-------+-------------------------------------------------------------------------------------------------------------------|
|   1 | 2019-12-12 |   71.5 |    67 |                                                                                                                   |
|   2 | 2019-12-13 |   71.5 |    62 |                                                                                                                   |
|   3 | 2019-12-14 |   71.5 |    57 |                                                                                                                   |
|   4 | 2019-12-15 |   71.5 |    52 |                                                                                                                   |
|   5 | 2019-12-16 |   71.5 |    48 |                                                                                                                   |
|   6 | 2019-12-17 |   71.5 |    43 |                                                                                                                   |
|   7 | 2019-12-18 |   71.5 |    38 |                                                                                                                   |
|   8 | 2019-12-19 |   67.0 |    33 | WRITE.1575286599                                                                                                  |
|   9 | 2019-12-20 |   67.0 |    29 |                                                                                                                   |
|  10 | 2019-12-21 |   67.0 |    24 |                                                                                                                   |
|  11 | 2019-12-22 |   56.0 |    19 | READ.1573405076                                                                                                   |
|  12 | 2019-12-23 |   56.0 |    14 |                                                                                                                   |
|  13 | 2019-12-24 |   48.0 |    10 | READ.1573385682                                                                                                   |
|  14 | 2019-12-25 |   48.0 |     5 |                                                                                                                   |
|  15 | 2019-12-26 |    0.0 |     0 | READ.1576157663 READ.1576218020 WRITE.1576217766 DEV.1576217466 WRITE.1576074953 WRITE.1576502914 READ.1576169089 |
#+END:
** TASK LIST
#+BEGIN: columnview :hlines 2 :maxlevel 5 :id "TASKS"
| ITEM                                                       | TASKID           | OWNER        | PRIORITY | TODO | ESTIMATED | ACTUAL |
|------------------------------------------------------------+------------------+--------------+----------+------+-----------+--------|
| TASKS                                                      |                  |              | B        |      |      71.5 | 69.246 |
|------------------------------------------------------------+------------------+--------------+----------+------+-----------+--------|
| akshay196                                                  |                  |              | B        |      |        15 |  14.13 |
| The Pragmatic Programmer: your journey to mastery - Part I | READ.1576169089  | akshay196    | B        | DONE |        15 |  14.13 |
|------------------------------------------------------------+------------------+--------------+----------+------+-----------+--------|
| bhavin192                                                  |                  |              | B        |      |      12.5 |  13.88 |
| Write blog post about EmacsConf 2019                       | WRITE.1575286599 | bhavin192    | B        | DONE |       4.5 |   4.83 |
| Write blog post about Ansible                              | WRITE.1576502914 | bhavin192    | B        | DONE |         8 |   9.05 |
|------------------------------------------------------------+------------------+--------------+----------+------+-----------+--------|
| gandalfdwite                                               |                  |              | B        |      |        15 |  15.74 |
| Linux Under the Hood                                       | READ.1573405076  | gandalfdwite | B        | DONE |        11 |  11.57 |
| Write a blog post on TravisCI                              | WRITE.1576074953 | gandalfdwite | B        | DONE |         4 |   4.17 |
|------------------------------------------------------------+------------------+--------------+----------+------+-----------+--------|
| kurianbenoy                                                |                  |              | B        |      |        16 | 16.946 |
| Swift course(FASTAI course)                                | DEV.1576217466   | kurianbenoy  | B        | DONE |         8 |   7.13 |
| Pytorch kaggle kernel & dlwpt -exercise                    | WRITE.1576217766 | kurianbenoy  | B        | DONE |         4 |  6.783 |
| Learn Emacs                                                | READ.1576218020  | kurianbenoy  | B        | DONE |         4 |  3.033 |
|------------------------------------------------------------+------------------+--------------+----------+------+-----------+--------|
| sandeepk                                                   |                  |              | B        |      |        13 |   8.55 |
| Fluent Python Part III                                     | READ.1573385682  | sandeepk     | B        | DONE |         8 |   4.72 |
| OAuth Training                                             | READ.1576157663  | sandeepk     | B        | DONE |         5 |   3.83 |
#+END:
* TASKS
  :PROPERTIES:
  :ID:       TASKS
  :SPRINTLENGTH: 15
  :SPRINTSTART: <2019-12-12 Thu>
  :wpd-akshay196: 1
  :wpd-bhavin192: 1
  :wpd-gandalfdwite: 1
  :wpd-kurianbenoy: 1
  :wpd-sandeepk: 1
  :END:
** akshay196
*** DONE The Pragmatic Programmer: your journey to mastery - Part I [4/4]
    CLOSED: [2019-12-26 Thu 19:35]
    :PROPERTIES:
    :ESTIMATED: 15
    :ACTUAL:   14.13
    :OWNER: akshay196
    :ID: READ.1576169089
    :TASKID: READ.1576169089
    :END:
    :LOGBOOK:
    CLOCK: [2019-12-26 Thu 18:26]--[2019-12-26 Thu 19:35] =>  1:09
    CLOCK: [2019-12-25 Wed 14:50]--[2019-12-25 Wed 14:53] =>  0:03
    CLOCK: [2019-12-25 Wed 14:34]--[2019-12-25 Wed 14:40] =>  0:06
    CLOCK: [2019-12-25 Wed 07:16]--[2019-12-25 Wed 07:30] =>  0:14
    CLOCK: [2019-12-25 Wed 00:07]--[2019-12-25 Wed 00:33] =>  0:26
    CLOCK: [2019-12-23 Mon 23:21]--[2019-12-24 Tue 00:27] =>  1:06
    CLOCK: [2019-12-23 Mon 12:59]--[2019-12-23 Mon 13:34] =>  0:35
    CLOCK: [2019-12-23 Mon 06:54]--[2019-12-23 Mon 07:42] =>  0:48
    CLOCK: [2019-12-22 Sun 06:51]--[2019-12-22 Sun 07:15] =>  0:24
    CLOCK: [2019-12-21 Sat 21:28]--[2019-12-21 Sat 22:16] =>  0:48
    CLOCK: [2019-12-19 Thu 22:29]--[2019-12-19 Thu 23:21] =>  0:52
    CLOCK: [2019-12-19 Thu 07:56]--[2019-12-19 Thu 08:30] =>  0:34
    CLOCK: [2019-12-18 Wed 20:41]--[2019-12-18 Wed 20:52] =>  0:11
    CLOCK: [2019-12-18 Wed 08:30]--[2019-12-18 Wed 09:03] =>  0:33
    CLOCK: [2019-12-17 Tue 20:04]--[2019-12-17 Tue 20:56] =>  0:52
    CLOCK: [2019-12-16 Mon 09:38]--[2019-12-16 Mon 10:12] =>  0:34
    CLOCK: [2019-12-15 Sun 22:31]--[2019-12-15 Sun 23:00] =>  0:29
    CLOCK: [2019-12-15 Sun 19:00]--[2019-12-15 Sun 19:46] =>  0:46
    CLOCK: [2019-12-14 Sat 19:18]--[2019-12-14 Sat 21:12] =>  1:54
    CLOCK: [2019-12-13 Fri 19:52]--[2019-12-13 Fri 21:36] =>  1:44
    :END:
    20th Anniversary Edition, 2nd Edition
    https://learning.oreilly.com/library/view/the-pragmatic-programmer/9780135956977/
    - [X] Chapter 1. A Pragmatic Philosophy        (2h)
    - [X] Chapter 2. A Pragmatic Approach          (2h)
    - [X] Chapter 3. The Basic Tools               (2h)
    - [X] Chapter 4. Pragmatic Paranoia            (2h)
** bhavin192
*** DONE Write blog post about EmacsConf 2019
    CLOSED: [2019-12-19 Thu 18:12]
    :PROPERTIES:
    :ESTIMATED: 4.5
    :ACTUAL:   4.83
    :OWNER:    bhavin192
    :ID:       WRITE.1575286599
    :TASKID:   WRITE.1575286599
    :END:
    :LOGBOOK:
    CLOCK: [2019-12-19 Thu 18:05]--[2019-12-19 Thu 18:12] =>  0:07
    CLOCK: [2019-12-19 Thu 14:30]--[2019-12-19 Thu 14:40] =>  0:10
    CLOCK: [2019-12-18 Wed 23:15]--[2019-12-19 Thu 00:27] =>  1:12
    CLOCK: [2019-12-18 Wed 19:35]--[2019-12-18 Wed 21:28] =>  1:53
    CLOCK: [2019-12-17 Tue 20:05]--[2019-12-17 Tue 20:46] =>  0:41
    CLOCK: [2019-12-17 Tue 19:20]--[2019-12-17 Tue 19:26] =>  0:06
    CLOCK: [2019-12-16 Mon 21:15]--[2019-12-16 Mon 21:21] =>  0:06
    CLOCK: [2019-12-16 Mon 19:28]--[2019-12-16 Mon 20:03] =>  0:35
    :END:
*** DONE Write blog post about Ansible [2/2]
    CLOSED: [2019-12-26 Thu 23:21]
    :PROPERTIES:
    :ESTIMATED: 8
    :ACTUAL:   9.05
    :OWNER:    bhavin192
    :ID:       WRITE.1576502914
    :TASKID:   WRITE.1576502914
    :END:
    :LOGBOOK:
    CLOCK: [2019-12-26 Thu 22:46]--[2019-12-26 Thu 23:21] =>  0:35
    CLOCK: [2019-12-26 Thu 20:05]--[2019-12-26 Thu 21:04] =>  0:59
    CLOCK: [2019-12-23 Mon 19:31]--[2019-12-23 Mon 19:45] =>  0:14
    CLOCK: [2019-12-23 Mon 18:45]--[2019-12-23 Mon 19:01] =>  0:16
    CLOCK: [2019-12-22 Sun 20:01]--[2019-12-22 Sun 20:12] =>  0:11
    CLOCK: [2019-12-22 Sun 18:51]--[2019-12-22 Sun 19:56] =>  1:05
    CLOCK: [2019-12-22 Sun 17:21]--[2019-12-22 Sun 18:29] =>  1:08
    CLOCK: [2019-12-22 Sun 13:57]--[2019-12-22 Sun 15:16] =>  1:19
    CLOCK: [2019-12-21 Sat 23:40]--[2019-12-22 Sun 00:52] =>  1:12
    CLOCK: [2019-12-21 Sat 11:59]--[2019-12-21 Sat 12:48] =>  0:49
    CLOCK: [2019-12-21 Sat 11:16]--[2019-12-21 Sat 11:40] =>  0:24
    CLOCK: [2019-12-20 Fri 23:47]--[2019-12-21 Sat 00:38] =>  0:51
    :END:
    - [X] Update the workstation setup playbook
    - [X] Write the blog post

** gandalfdwite
*** DONE Linux Under the Hood [7/7]
    CLOSED: [2019-12-22 Sun 02:04]
    :PROPERTIES:
    :ESTIMATED: 11
    :ACTUAL:   11.57
    :OWNER: gandalfdwite
    :ID: READ.1573405076
    :TASKID: READ.1573405076
    :END:
    :LOGBOOK:
    CLOCK: [2019-12-22 Sun 11:15]--[2019-12-22 Sun 12:25] =>  1:10
    CLOCK: [2019-12-21 Sat 19:25]--[2019-12-21 Sat 20:51] =>  1:26
    CLOCK: [2019-12-20 Fri 23:00]--[2019-12-21 Sat 00:15] =>  1:15
    CLOCK: [2019-12-18 Wed 21:30]--[2019-12-18 Wed 22:30] =>  1:00
    CLOCK: [2019-12-17 Tue 22:25]--[2019-12-17 Tue 23:51] =>  1:26
    CLOCK: [2019-12-15 Sun 15:07]--[2019-12-15 Sun 16:51] =>  1:44
    CLOCK: [2019-12-14 Sat 11:55]--[2019-12-14 Sat 13:00] =>  1:05
    CLOCK: [2019-12-13 Fri 21:03]--[2019-12-13 Fri 22:00] =>  0:57
    CLOCK: [2019-12-12 Thu 22:20]--[2019-12-12 Thu 23:51] =>  1:31

    :END:
    - [X] 7. Understanding processes                     ( 2h )
    - [X] 8. Security                                    ( 3h )
    - [X] 9. Hardware Initilization                      ( 1h )
    - [X] 10. Looking closer at the kernel               ( 2h )
    - [X] 11. Networking                                 ( 1h )
    - [X] 12. Performance optimization                   ( 1h )
    - [X] 13. Future of Linux                            ( 1h )

*** DONE Write a blog post on TravisCI [1/1]
    CLOSED: [2019-12-26 Thu 12:55]
    :PROPERTIES:
    :ESTIMATED: 4
    :ACTUAL:   4.17
    :OWNER:    gandalfdwite
    :ID:       WRITE.1576074953
    :TASKID:   WRITE.1576074953
    :END:
    :LOGBOOK:
    CLOCK: [2019-12-25 Wed 12:55]--[2019-12-25 Wed 15:10] =>  2:15
    CLOCK: [2019-12-24 Tue 20:30]--[2019-12-24 Tue 22:25] =>  1:55
    :END:

    - [X] Write blog post       ( 4h )
** kurianbenoy
*** DONE Swift course(FASTAI course)
    CLOSED: [2019-12-26 Sun 23:00]
    :PROPERTIES:
  :ESTIMATED: 8
  :ACTUAL: 7.13
  :OWNER: kurianbenoy
  :ID: DEV.1576217466
  :TASKID: DEV.1576217466
  :END:
    :LOGBOOK:
  CLOCK: [2019-12-19 Thu 13:06]--[2019-12-19 Thu 14:23] =>  1:17
  CLOCK: [2019-12-19 Thu 10:18]--[2019-12-19 Thu 10:39] =>  0:21
  CLOCK: [2019-12-14 Sat 07:00]--[2019-12-14 Sat 10:00] =>  3:00
  CLOCK: [2019-12-15 Sun 15:00]--[2019-12-15 Sun 17:30] =>  2:30
  :END:
  - [X] Lesson 13 (3h)
  - [X] Lesson 14 (3h)
  - [X] Blog post about Swift language and first tutorial
    https://kurianbenoy.github.io/2019-12-19-swift4tensorflowintro/
*** DONE Pytorch kaggle kernel & dlwpt -exercise
    CLOSED: [2019-12-26 Sun 23:00]
    :PROPERTIES:
   :ESTIMATED: 4
   :ACTUAL: 6.783
   :OWNER: kurianbenoy
   :ID: WRITE.1576217766
   :TASKID: WRITE.1576217766
   :END:
    :LOGBOOK:
   CLOCK: [2019-12-23 Mon 07:46]--[2019-12-23 Mon 08:49] =>  1:03
   CLOCK: [2019-12-23 Mon 07:00]--[2019-12-23 Mon 07:46] =>  0:46
   CLOCK: [2019-12-22 Sun 22:35]--[2019-12-23 Mon 00:10] =>  1:35
   CLOCK: [2019-12-22 Sun 19:18]--[2019-12-22 Sun 20:04] =>  0:46
   CLOCK: [2019-12-21 Sat 12:29]--[2019-12-21 Sat 13:27] =>  0:58
   CLOCK: [2019-12-21 Sat 12:03]--[2019-12-21 Sat 12:09] =>  0:06
   CLOCK: [2019-12-21 Sat 10:19]--[2019-12-21 Sat 11:00] =>  0:41
   CLOCK: [2019-12-21 Sat 00:04]--[2019-12-21 Sat 00:56] =>  0:52
   :END:
*** DONE Learn Emacs
    CLOSED: [2019-12-26 Sun 23:00]
    :PROPERTIES:
   :ESTIMATED: 4
   :ACTUAL: 3.033
   :OWNER: kurianbenoy
   :ID: READ.1576218020
   :TASKID: READ.1576218020
   :END:
    :LOGBOOK:
   CLOCK: [2019-12-25 Wed 10:48]--[2019-12-25 Wed 11:15] =>  0:28
   CLOCK: [2019-12-25 Wed 07:35]--[2019-12-25 Wed 08:13] =>  0:38
   CLOCK: [2019-12-24 Tue 10:32]--[2019-12-24 Tue 11:34] =>  1:02
   CLOCK: [2019-12-20 Fri 22:39]--[2019-12-20 Fri 23:07] =>  0:28
   CLOCK: [2019-12-20 Fri 21:42]--[2019-12-20 Fri 22:30] =>  0:48
   CLOCK: [2019-12-17 Tue 19:35]--[2019-12-17 Tue 19:42] =>  0:07
   CLOCK: [2019-12-17 Tue 14:38]--[2019-12-17 Tue 15:25] =>  0:47
   :END:
    https://geeksocket.in/posts/entering-church-emacs/#references
** sandeepk
*** DONE Fluent Python Part III [1/1]
    CLOSED: [2019-12-24 Tue 00:12]
    :PROPERTIES:
    :ESTIMATED: 8
    :ACTUAL:   4.72
    :OWNER: sandeepk
    :ID: READ.1573385682
    :TASKID: READ.1573385682
    :END:
    :LOGBOOK:
    CLOCK: [2019-12-23 Mon 23:30]--[2019-12-24 Tue 00:12] =>  0:42
    CLOCK: [2019-12-23 Mon 07:30]--[2019-12-23 Mon 08:50] =>  1:20
    CLOCK: [2019-12-21 Sat 18:00]--[2019-12-21 Sat 18:40] =>  0:40
    CLOCK: [2019-12-21 Sat 12:00]--[2019-12-21 Sat 13:00] =>  1:00
    CLOCK: [2019-12-18 Wed 20:44]--[2019-12-18 Wed 21:45] =>  1:01
    :END:
    - [X] Chapter-6  Design Patterns with First-Class Functions Part II (4h)
*** DONE OAuth Training [2/2]
    CLOSED: [2019-12-26 Thu 20:20]
    :PROPERTIES:
    :ESTIMATED: 5
    :ACTUAL:   3.83
    :OWNER: sandeepk
    :ID: READ.1576157663
    :TASKID: READ.1576157663
    :END:
    :LOGBOOK:
    CLOCK: [2019-12-26 Thu 20:00]--[2019-12-26 Thu 20:20] =>  0:20
    CLOCK: [2019-12-26 Thu 08:30]--[2019-12-26 Thu 09:20] =>  0:50
    CLOCK: [2019-12-25 Wed 22:00]--[2019-12-25 Wed 22:30] =>  0:30
    CLOCK: [2019-12-12 Thu 00:10]--[2019-12-12 Thu 02:20] =>  2:10
    :END:
    - [X] Watch OAuth Videos  (3h)
    - [X] Exercise 1          (2h)
