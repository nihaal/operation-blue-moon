#+TITLE: July 22, 2020 - August 2, 2020 (12 days)
#+PROPERTY: Effort_ALL 0 0:05 0:10 0:30 1:00 2:00 3:00 4:00
#+COLUMNS: %35ITEM %TASKID %OWNER %3PRIORITY %TODO %5ESTIMATED{+} %3ACTUAL{+}
* REPORTS
** SCRUM BOARD
#+BEGIN: block-update-board
#+END:
** DEVELOPER SUMMARY
#+BEGIN: block-update-summary
#+END:
** BURNDOWN CHART
#+BEGIN: block-update-graph
#+END:
** BURNDOWN LIST
#+PLOT: title:"Burndown" ind:1 deps:(3 4) set:"term dumb" set:"xtics scale 0.5" set:"ytics scale 0.5" file:"burndown.plt" set:"xrange [0:17]"
#+BEGIN: block-update-burndown
#+END:
** TASK LIST
#+BEGIN: columnview :hlines 2 :maxlevel 5 :id "TASKS"
#+END:
* TASKS
  :PROPERTIES:
  :ID:       TASKS
  :SPRINTLENGTH: 12
  :SPRINTSTART: <2020-07-22 Wed>
  :wpd-akshay196: 1
  :wpd-bhavin192: 1
  :wpd-gandalfdwite: 1
  :END:
** akshay196
*** DONE The Go Programming Language - Part V [14/14]
    CLOSED: [2020-08-02 Sun 19:05]
   :PROPERTIES:
   :ESTIMATED: 12
   :ACTUAL:   11.90
   :OWNER:    akshay196
   :ID:       READ.1587566538
   :TASKID:   READ.1587566538
   :END:
   :LOGBOOK:
   CLOCK: [2020-08-02 Sun 18:25]--[2020-08-02 Sun 19:05] =>  0:40
   CLOCK: [2020-08-02 Sun 09:10]--[2020-08-02 Sun 10:50] =>  1:40
   CLOCK: [2020-08-01 Sat 20:04]--[2020-08-01 Sat 20:21] =>  0:17
   CLOCK: [2020-08-01 Sat 18:07]--[2020-08-01 Sat 18:31] =>  0:24
   CLOCK: [2020-07-31 Fri 21:54]--[2020-07-31 Fri 23:02] =>  1:08
   CLOCK: [2020-07-31 Fri 08:41]--[2020-07-31 Fri 09:10] =>  0:29
   CLOCK: [2020-07-30 Thu 09:02]--[2020-07-30 Thu 09:29] =>  0:27
   CLOCK: [2020-07-28 Tue 21:20]--[2020-07-28 Tue 22:26] =>  1:06
   CLOCK: [2020-07-27 Mon 19:01]--[2020-07-27 Mon 20:00] =>  0:59
   CLOCK: [2020-07-26 Sun 22:00]--[2020-07-26 Sun 23:28] =>  1:28
   CLOCK: [2020-07-26 Sun 00:04]--[2020-07-26 Sun 00:40] =>  0:36
   CLOCK: [2020-07-25 Sat 08:51]--[2020-07-25 Sat 10:04] =>  1:13
   CLOCK: [2020-07-23 Thu 19:22]--[2020-07-23 Thu 19:50] =>  0:28
   CLOCK: [2020-07-22 Wed 21:50]--[2020-07-22 Wed 22:49] =>  0:59
   :END:
   https://learning.oreilly.com/library/view/the-go-programming/9780134190570/
    - [X] Chapter 9.5. Lazy Initialization: sync.Once                       (120m)
    - [X] Chapter 9.6. The Race Detector                                    ( 30m)
    - [X] Chapter 9.7. Example: Concurrent Non-Blocking Cache               (120m)
    - [X] Chapter 9.8. Goroutines and Threads                               ( 60m)
    - [X] Chapter 10.  Packages and the Go Tool                             ( 15m)
    - [X] Chapter 10.1. Introduction                                        ( 15m)
    - [X] Chapter 10.2. Import Paths                                        ( 30m)
    - [X] Chapter 10.3. The Package Declaration                             ( 30m)
    - [X] Chapter 10.4. Import Declarations                                 ( 30m)
    - [X] Chapter 10.5. Blank Imports                                       ( 60m)
    - [X] Chapter 10.6. Packages and Naming                                 ( 60m)
    - [X] Chapter 10.7. The Go Tool                                         (120m)
    - [X] Chapter 11.  Testing                                              ( 15m)
    - [X] Chapter 11.1. The go test Tool                                    ( 15m)

** bhavin192
*** DONE Watch Camp Cloud Native (June 2020) - Part I [22/22]
    CLOSED: [2020-08-02 Sun 17:27]
    :PROPERTIES:
    :ESTIMATED: 12
    :ACTUAL:   10.65
    :OWNER:    bhavin192
    :ID:       READ.1595437761
    :TASKID:   READ.1595437761
    :END:
    :LOGBOOK:
    CLOCK: [2020-08-02 Sun 16:22]--[2020-08-02 Sun 17:27] =>  1:05
    CLOCK: [2020-08-02 Sun 15:20]--[2020-08-02 Sun 16:06] =>  0:46
    CLOCK: [2020-08-02 Sun 13:33]--[2020-08-02 Sun 14:30] =>  0:57
    CLOCK: [2020-08-01 Sat 17:12]--[2020-08-01 Sat 17:39] =>  0:27
    CLOCK: [2020-08-01 Sat 15:54]--[2020-08-01 Sat 16:12] =>  0:18
    CLOCK: [2020-08-01 Sat 14:50]--[2020-08-01 Sat 15:10] =>  0:20
    CLOCK: [2020-08-01 Sat 13:35]--[2020-08-01 Sat 14:15] =>  0:40
    CLOCK: [2020-07-30 Thu 22:07]--[2020-07-30 Thu 23:06] =>  0:59
    CLOCK: [2020-07-29 Wed 22:20]--[2020-07-29 Wed 22:31] =>  0:11
    CLOCK: [2020-07-28 Tue 19:19]--[2020-07-28 Tue 20:22] =>  1:03
    CLOCK: [2020-07-27 Mon 20:14]--[2020-07-27 Mon 21:16] =>  1:02
    CLOCK: [2020-07-26 Sun 11:58]--[2020-07-26 Sun 13:40] =>  1:42
    CLOCK: [2020-07-23 Thu 19:44]--[2020-07-23 Thu 20:53] =>  1:09
    :END:
    - [X] [[https://www.youtube.com/watch?v=AV44uY6t9GI][How End Users Really Adopt Cloud Native: The CNCF Technology Radar - Cheryl Hung]]                        (50m)
    - [X] [[https://www.youtube.com/watch?v=FI4OZn0bazA][How a K8s IDE Can Increase Developer Productivity - Jussi Nummelin]]                                      (20m)
    - [X] [[https://www.youtube.com/watch?v=JxkQQwOCMUs][The Building Blocks of DX: K8s Evolution From CLI to GitOps - Katie Gamanji]]                             (50m)
    - [X] [[https://www.youtube.com/watch?v=qZsd2ay4uwg][Demystifying Cloud Native Buildpacks ~ Developer's Life Without Dockerfile !! - Suman Chakraborty]]       (30m)
    - [X] [[https://www.youtube.com/watch?v=IJbf_FaSuLY][Your Own Kubernetes Operator: Not Only in Go - Nicolas Frankel]]                                          (30m)
    - [X] [[https://www.youtube.com/watch?v=doDPaA43SSw][Vendor Agnostic Instrumentation With The OpenTelemetry Collector - Paul Osman]]                           (30m)
    - [X] [[https://www.youtube.com/watch?v=bWIImgCuC6I][How to Migrate an Existing Application to Serverless? - Marcia Villalba]]                                 (30m)
    - [X] [[https://www.youtube.com/watch?v=uBtHP1rLVPU][Enter the Next Level - Migrating to Cloud Native Platforms - Daniel Kocot]]                               (30m)
    - [X] [[https://www.youtube.com/watch?v=dAScAs6Hsb0][Being More Fluent With Your Logs - Phil Wilkins]]                                                         (30m)
    - [X] [[https://www.youtube.com/watch?v=g9C4-i5e8kw][Fireside Chat: SMI and the Future of Service Mesh - Lee Calcote, Bridget Kromhout, & Alex Williams]]      (30m)
    - [X] [[https://www.youtube.com/watch?v=fQw-nXTy2r8][OAuth 2.0 and OpenID Connect (In Plain English) - Micah Silverman]]                                       (30m)
    - [X] [[https://www.youtube.com/watch?v=CR8UugOaTL8][App (Plat) Sampler: the Cloud Native Adventure - Ralph Squillace]]                                        (25m)
    - [X] [[https://www.youtube.com/watch?v=9gv8cx6v_4o][Creating Clusters with Cluster API - Gianluca Arbezzano & Ria Bhatia]]                                    (30m)
    - [X] [[https://www.youtube.com/watch?v=3NKU-oWJ8sI][A Cloud Native User Tale in WSL2 Lands - Nuno do Carmo]]                                                  (20m)
    - [X] [[https://www.youtube.com/watch?v=-bjit7CKxt4][Kubernetes Past, Present and Future - Brendan Burns]]                                                     (50m)
    - [X] [[https://www.youtube.com/watch?v=R3ZVyj9TTvQ][Achieving Decentralized Zen in the Kubernetes Era - Reza Shafii]]                                         (20m)
    - [X] [[https://www.youtube.com/watch?v=yec1967Poyw][Panel: Kubernetes Best Practices]]                                                                        (45m)
    - [X] [[https://www.youtube.com/watch?v=BQ76oxthNog][Service Mesh Building Blocks with Linkerd - Charles Pretzer]]                                             (30m)
    - [X] [[https://www.youtube.com/watch?v=2H3DtdkUYAk][Building Stateful Workloads in Kubernetes - Rob Richardson]]                                              (30m)
    - [X] [[https://www.youtube.com/watch?v=sPLjqKoNcMk][Advanced Feature Flagging: It's All About The Data - Dave Karow]]                                         (25m)
    - [X] [[https://www.youtube.com/watch?v=p1lxM-BuPSQ][The Practicalities of Deploying eBPF - John Armstrong]]                                                   (25m)
    - [X] [[https://www.youtube.com/watch?v=oH9PBqmgAQs][honkCTL: Taking Jokes Too Far - Jeffrey Sica]]                                                            (50m)

** gandalfdwite
*** DONE The Go Programming Language - Part II [4/4]
    CLOSED: [2020-08-03 Mon 07:59]
    :PROPERTIES:
    :ESTIMATED: 12
    :ACTUAL:   12.15
    :OWNER:    gandalfdwite
    :ID:       READ.1593833077
    :TASKID:   READ.1593833077
    :END:
    :LOGBOOK:
    CLOCK: [2020-08-02 Sun 11:35]--[2020-08-02 Sun 12:59] =>  1:24
    CLOCK: [2020-08-01 Sat 09:33]--[2020-08-01 Sat 10:40] =>  1:07
    CLOCK: [2020-07-30 Thu 19:35]--[2020-07-30 Thu 21:11] =>  1:36
    CLOCK: [2020-07-29 Wed 20:46]--[2020-07-29 Wed 22:15] =>  1:29
    CLOCK: [2020-07-28 Tue 18:10]--[2020-07-28 Tue 19:16] =>  1:06
    CLOCK: [2020-07-26 Sun 13:02]--[2020-07-26 Sun 14:29] =>  1:27
    CLOCK: [2020-07-25 Sat 18:00]--[2020-07-25 Sat 19:41] =>  1:41
    CLOCK: [2020-07-24 Fri 18:05]--[2020-07-24 Fri 19:00] =>  0:55
    CLOCK: [2020-07-23 Thu 21:55]--[2020-07-23 Thu 23:19] =>  1:24
    :END:
    - [X] Methods                        ( 3h )
    - [X] Interfaces                     ( 3h )
    - [X] Goroutines and Channels        ( 3h )
    - [X] Concurrency with Shared Variables  ( 3h )
