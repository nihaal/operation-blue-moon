#+TITLE: January 10-25, 2020 (16 days)
#+PROPERTY: Effort_ALL 0 0:05 0:10 0:30 1:00 2:00 3:00 4:00
#+COLUMNS: %35ITEM %TASKID %OWNER %3PRIORITY %TODO %5ESTIMATED{+} %3ACTUAL{+}
* REPORTS
** SCRUM BOARD
#+BEGIN: block-update-board
#+END:
** DEVELOPER SUMMARY
#+BEGIN: block-update-summary
#+END:
** BURNDOWN CHART
#+BEGIN: block-update-graph
#+END:
** BURNDOWN LIST
#+PLOT: title:"Burndown" ind:1 deps:(3 4) set:"term dumb" set:"xtics scale 0.5" set:"ytics scale 0.5" file:"burndown.plt" set:"xrange [0:17]"
#+BEGIN: block-update-burndown
#+END:
** TASK LIST
#+BEGIN: columnview :hlines 2 :maxlevel 5 :id "TASKS"
#+END:
* TASKS
  :PROPERTIES:
  :ID:       TASKS
  :SPRINTLENGTH: 16
  :SPRINTSTART: <2020-01-10 Fri>
  :wpd-bhavin192: 1
  :wpd-gandalfdwite: 1
  :wpd-kurianbenoy: 4
  :END:
** bhavin192
*** DONE Write blog post about Helm v3
    CLOSED: [2020-01-25 Sat 23:59]
    :PROPERTIES:
    :ESTIMATED: 8
    :ACTUAL:   3.23
    :OWNER:    bhavin192
    :ID:       WRITE.1577718004
    :TASKID:   WRITE.1577718004
    :END:
    :LOGBOOK:
    CLOCK: [2020-01-25 Sat 22:17]--[2020-01-25 Sat 23:59] =>  1:42
    CLOCK: [2020-01-23 Thu 19:15]--[2020-01-23 Thu 19:19] =>  0:04
    CLOCK: [2020-01-22 Wed 20:54]--[2020-01-22 Wed 21:04] =>  0:10
    CLOCK: [2020-01-22 Wed 19:24]--[2020-01-22 Wed 19:41] =>  0:17
    CLOCK: [2020-01-20 Mon 21:54]--[2020-01-20 Mon 22:10] =>  0:16
    CLOCK: [2020-01-15 Wed 20:15]--[2020-01-15 Wed 21:00] =>  0:45
    :END:
*** DONE CNCF Project Webinar: A Conversation about Helm 3
    CLOSED: [2020-01-15 Wed 00:33]
    :PROPERTIES:
    :ESTIMATED: 1
    :ACTUAL:   1.07
    :OWNER:    bhavin192
    :ID:       EVENT.1578852275
    :TASKID:   EVENT.1578852275
    :END:
    :LOGBOOK:
    CLOCK: [2020-01-14 Tue 23:29]--[2020-01-15 Wed 00:33] =>  1:04
    :END:
    [[https://zoom.us/webinar/register/1515768748019/WN_NhXnvAT_RQiNJr7LMn4ILA]]
*** DONE Attend Rootconf Delhi 2020
    CLOSED: [2020-01-18 Sat 17:30]
    :PROPERTIES:
    :ESTIMATED: 7
    :ACTUAL:   8.00
    :OWNER:    bhavin192
    :ID:       EVENT.1578852044
    :TASKID:   EVENT.1578852044
    :END:
    :LOGBOOK:
    CLOCK: [2020-01-18 Sat 09:30]--[2020-01-18 Sat 17:30] =>  8:00
    :END:

** gandalfdwite
*** DONE Blog Post on year 2019 and OBM [1/1]
    CLOSED: [2020-01-25 Sat 20:43]
   :PROPERTIES:
   :ESTIMATED: 4
   :ACTUAL:   4.17
   :OWNER: gandalfdwite
   :ID: WRITE.1578812481
   :TASKID: WRITE.1578812481
   :END:
   :LOGBOOK:
   CLOCK: [2020-01-25 Sat 19:52]--[2020-01-25 Sat 20:42] =>  0:50
   CLOCK: [2020-01-24 Fri 21:05]--[2020-01-24 Fri 22:35] =>  1:30
   CLOCK: [2020-01-23 Thu 17:25]--[2020-01-23 Thu 18:25] =>  1:00
   :END:
   - [X] Write blog post on year 2019    ( 4h )
*** DONE Containerize pravarag dot com [4/4]
    CLOSED: [2020-01-23 Thu 07:50]
    :PROPERTIES:
    :ESTIMATED: 12
    :ACTUAL:   13.83
    :OWNER: gandalfdwite
    :ID: DEV.1578812553
    :TASKID: DEV.1578812553
    :END:
    :LOGBOOK:
    CLOCK: [2020-01-22 Wed 23:05]--[2020-01-22 Wed 23:35] =>  0:30
    CLOCK: [2020-01-21 Tue 17:30]--[2020-01-21 Tue 18:35] =>  1:05
    CLOCK: [2020-01-20 Mon 22:40]--[2020-01-20 Mon 23:35] =>  0:55
    CLOCK: [2020-01-19 Sun 10:40]--[2020-01-19 Sun 12:45] =>  2:05
    CLOCK: [2020-01-18 Sat 14:50]--[2020-01-18 Sat 16:35] =>  1:45
    CLOCK: [2020-01-17 Fri 20:40]--[2020-01-17 Fri 21:35] =>  0:55
    CLOCK: [2020-01-16 Thu 19:15]--[2020-01-16 Thu 20:15] =>  1:00
    CLOCK: [2020-01-15 Wed 22:30]--[2020-01-15 Wed 23:30] =>  1:00
    CLOCK: [2020-01-13 Mon 20:20]--[2020-01-13 Mon 21:35] =>  1:15
    CLOCK: [2020-01-12 Sun 11:00]--[2020-01-12 Sun 11:55] =>  0:55
    CLOCK: [2020-01-11 Sat 13:15]--[2020-01-11 Sat 14:35] =>  1:20
    CLOCK: [2020-01-10 Fri 21:05]--[2020-01-10 Fri 22:10] =>  1:05

    :END:
    - [X] Write Nginx based dockerfiles     ( 2h )
    - [X] CI/CD pipeline to enable code     ( 4h )
          changes and updating docker
          images
    - [X] Write K8s based deployment specs  ( 4h )
          as helm charts
    - [X] CI/CD to update Helm charts       ( 2h )

** kurianbenoy
*** DONE Compete in Kaggle DSBowl competition - Part2
   :PROPERTIES:
   :ESTIMATED: 15
   :ACTUAL: 18
   :OWNER: kurianbenoy
   :ID: DEV.1578594699
   :TASKID: DEV.1578594699
   :END:
   :LOGBOOK:
   CLOCK: [2020-01-20 Mon 21:53]--[2020-01-20 Mon 23:20] =>  1:27
   CLOCK: [2020-01-19 Sun 23:38]--[2020-01-19 Sun 23:50] =>  0:12
   CLOCK: [2020-01-19 Sun 10:02]--[2020-01-19 Sun 11:42] =>  1:40
   CLOCK: [2020-01-19 Sun 08:08]--[2020-01-19 Sun 09:40] =>  1:32
   CLOCK: [2020-01-18 Sat 20:05]--[2020-01-18 Sat 23:59] =>  3:54
   CLOCK: [2020-01-16 Thu 11:20]--[2020-01-16 Thu 11:56] =>  0:36
   CLOCK: [2020-01-15 Wed 12:39]--[2020-01-15 Wed 13:48] =>  1:09
   CLOCK: [2020-01-14 Tue 19:30]--[2020-01-14 Tue 22:13] =>  2:47
   CLOCK: [2020-01-13 Mon 15:33]--[2020-01-13 Mon 16:15] =>  0:42
   CLOCK: [2020-01-13 Mon 15:01]--[2020-01-13 Mon 15:30] =>  0:29
   CLOCK: [2020-01-13 Mon 05:44]--[2020-01-13 Mon 06:05] =>  0:21
   CLOCK: [2020-01-12 Sun 08:21]--[2020-01-12 Sun 08:57] =>  0:36
   CLOCK: [2020-01-11 Sat 21:32]--[2020-01-11 Sat 23:59] =>  2:27
   CLOCK: [2020-01-10 Fri 12:24]--[2020-01-10 Fri 13:30] =>  1:06
   CLOCK: [2020-01-10 Fri 11:19]--[2020-01-10 Fri 12:00] =>  0:41
   :END:
   - [X] Try lightGM model
   - [ ] Stack top scoring models
   - [X] Create local CV
*** DONE Prepare for GATE 2020 - Part2
   :PROPERTIES:
   :ESTIMATED: 26
   :ACTUAL:
   :OWNER: kurianbenoy
   :ID: READ.1578594810
   :TASKID: READ.1578594810
   :END:
   :LOGBOOK:
   CLOCK: [2020-01-22 Wed 19:58]--[2020-01-22 Wed 20:22] =>  0:24
   CLOCK: [2020-01-21 Tue 19:43]--[2020-01-21 Tue 21:00] =>  1:17
   CLOCK: [2020-01-21 Tue 06:33]--[2020-01-21 Tue 06:49] =>  0:16
   CLOCK: [2020-01-21 Tue 04:52]--[2020-01-21 Tue 06:24] =>  1:32
   CLOCK: [2020-01-20 Mon 20:20]--[2020-01-20 Mon 21:05] =>  0:45
   CLOCK: [2020-01-20 Mon 19:34]--[2020-01-20 Mon 20:14] =>  0:40
   CLOCK: [2020-01-20 Mon 05:10]--[2020-01-20 Mon 07:10] =>  2:00
   CLOCK: [2020-01-19 Sun 22:58]--[2020-01-19 Sun 23:31] =>  0:33
   CLOCK: [2020-01-19 Sun 20:26]--[2020-01-19 Sun 22:20] =>  1:56
   CLOCK: [2020-01-18 Sat 16:36]--[2020-01-18 Sat 17:37] =>  1:01
   CLOCK: [2020-01-18 Sat 14:30]--[2020-01-18 Sat 16:07] =>  1:37
   CLOCK: [2020-01-17 Fri 04:53]--[2020-01-17 Fri 06:03] =>  1:10
   CLOCK: [2020-01-16 Thu 21:18]--[2020-01-16 Thu 23:18] =>  2:00
   CLOCK: [2020-01-16 Thu 06:50]--[2020-01-16 Thu 07:36] =>  0:46
   CLOCK: [2020-01-16 Thu 05:07]--[2020-01-16 Thu 06:25] =>  1:18
   CLOCK: [2020-01-14 Tue 17:32]--[2020-01-14 Tue 18:00] =>  0:28
   CLOCK: [2020-01-14 Tue 17:28]--[2020-01-14 Tue 17:32] =>  0:04
   CLOCK: [2020-01-14 Tue 15:56]--[2020-01-14 Tue 16:24] =>  0:28
   CLOCK: [2020-01-14 Tue 15:46]--[2020-01-14 Tue 15:56] =>  0:10
   CLOCK: [2020-01-14 Tue 05:44]--[2020-01-14 Tue 07:19] =>  1:35
   CLOCK: [2020-01-13 Mon 23:01]--[2020-01-14 Tue 00:50] =>  1:49
   CLOCK: [2020-01-13 Mon 21:52]--[2020-01-13 Mon 22:05] =>  0:13
   CLOCK: [2020-01-13 Mon 08:08]--[2020-01-13 Mon 08:22] =>  0:14
   CLOCK: [2020-01-13 Mon 07:08]--[2020-01-12 Mon 07:15] =>  0:07
   CLOCK: [2020-01-13 Mon 06:28]--[2020-01-13 Mon 07:02] =>  0:34
   CLOCK: [2020-01-12 Sun 23:22]--[2020-01-13 Mon 00:16] =>  0:54
   CLOCK: [2020-01-12 Sun 22:42]--[2020-01-12 Sun 23:00] =>  0:18
   CLOCK: [2020-01-12 Sun 21:09]--[2020-01-12 Sun 21:16] =>  0:07
   CLOCK: [2020-01-12 Sun 20:35]--[2020-01-12 Sun 21:05] =>  0:30
   CLOCK: [2020-01-12 Sun 19:08]--[2020-01-12 Sun 20:31] =>  1:23
   CLOCK: [2020-01-12 Sun 10:17]--[2020-01-12 Sun 10:57] =>  0:40
   CLOCK: [2020-01-12 Sun 08:57]--[2020-01-12 Sun 09:37] =>  0:40
   CLOCK: [2020-01-11 Sat 19:43]--[2020-01-11 Sat 20:47] =>  1:04
   CLOCK: [2020-01-11 Sat 18:10]--[2020-01-11 Sat 19:31] =>  1:21
   CLOCK: [2020-01-10 Fri 10:44]--[2020-01-10 Fri 11:13] =>  0:29
   CLOCK: [2020-01-10 Fri 10:06]--[2020-01-10 Fri 10:20] =>  0:14
   CLOCK: [2020-01-10 Fri 09:21]--[2020-01-10 Fri 10:00] =>  0:39
   CLOCK: [2020-01-10 Fri 08:49]--[2020-01-10 Fri 09:21] =>  0:32
   :END:
    - [ ] TOC
    - [X] Compiler design
    - [X] Databases
    - [ ] Digital Logic
    - [X] Datastructures and programming*
*** DONE IN_PROGRESS Read CTCI part-3
    :PROPERTIES:
    :ESTIMATED: 5
    :ACTUAL:
    :OWNER: kurianbenoy
    :ID: READ.1578594906
    :TASKID: READ.1578594906
    :END:
    :LOGBOOK:
    CLOCK: [2020-01-31 Fri 07:10]--[2020-01-31 Fri 08:30] =>  1:20
    CLOCK: [2020-01-28 Tue 07:00]--[2020-01-28 Tue 08:15] =>  1:15
    CLOCK: [2020-01-21 Tue 10:24]--[2020-01-21 Tue 10:42] =>  0:18
    :END:
    - [ ] Dynammic Programming
*** DONE Project: Malayalam text to speech system
   :PROPERTIES:
    :ESTIMATED: 18
    :ACTUAL:
    :OWNER: kurianbenoy
    :ID: DEV.1578595021
    :TASKID: DEV.1578595021
    :END:
   :LOGBOOK:
   CLOCK: [2020-01-28 Tue 12:43]--[2020-01-28 Tue 14:30] =>  1:47
   CLOCK: [2020-01-23 Thu 10:58]--[2020-01-23 Thu 13:58] =>  3:00
   CLOCK: [2020-01-23 Thu 04:51]--[2020-01-23 Thu 07:03] =>  2:12
   CLOCK: [2020-01-22 Wed 13:50]--[2020-01-22 Wed 16:05] =>  2:15
   CLOCK: [2020-01-22 Wed 06:55]--[2020-01-22 Wed 07:20] =>  0:25
   CLOCK: [2020-01-21 Tue 15:30]--[2020-01-21 Tue 16:15] =>  0:45
   CLOCK: [2020-01-21 Tue 13:45]--[2020-01-21 Tue 15:00] =>  1:15
   CLOCK: [2020-01-21 Tue 10:30]--[2020-01-21 Tue 11:20] =>  0:50
   CLOCK: [2020-01-20 Mon 14:20]--[2020-01-20 Mon 16:02] =>  1:42
   CLOCK: [2020-01-20 Mon 09:00]--[2020-01-20 Mon 09:33] =>  0:33
   CLOCK: [2020-01-18 Sat 04:41]--[2020-01-18 Sat 07:02] =>  2:21
   CLOCK: [2020-01-17 Fri 10:01]--[2020-01-17 Fri 15:32] =>  5:31
   CLOCK: [2020-01-16 Thu 11:56]--[2020-01-16 Thu 12:30] =>  0:34
   CLOCK: [2020-01-16 Thu 10:06]--[2020-01-16 Thu 11:20] =>  1:14
   CLOCK: [2020-01-15 Wed 22:49]--[2020-01-15 Wed 15:30] =>  0:11
   CLOCK: [2020-01-15 Wed 15:46]--[2020-01-15 Wed 15:56] =>  0:10
   CLOCK: [2020-01-15 Wed 15:11]--[2020-01-15 Wed 15:14] =>  0:03
   CLOCK: [2020-01-15 Wed 14:05]--[2020-01-15 Wed 15:05] =>  1:00 
   CLOCK: [2020-01-13 Mon 13:42]--[2020-01-13 Mon 13:56] =>  0:14
   CLOCK: [2020-01-11 Sat 15:06]--[2020-01-11 Sat 16:30] =>  1:24
   CLOCK: [2020-01-10 Fri 15:41]--[2020-01-10 Fri 23:24] =>  7:43
   :END:
   - [X] Write survey paper
   - [X] complete making inital TTS system

     
