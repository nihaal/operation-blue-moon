#+TITLE: September 18-30, 2020 (13 days)
#+PROPERTY: Effort_ALL 0 0:05 0:10 0:30 1:00 2:00 3:00 4:00
#+COLUMNS: %35ITEM %TASKID %OWNER %3PRIORITY %TODO %5ESTIMATED{+} %3ACTUAL{+}
* REPORTS
** SCRUM BOARD
#+BEGIN: block-update-board
#+END:
** DEVELOPER SUMMARY
#+BEGIN: block-update-summary
#+END:
** BURNDOWN CHART
#+BEGIN: block-update-graph
#+END:
** BURNDOWN LIST
#+PLOT: title:"Burndown" ind:1 deps:(3 4) set:"term dumb" set:"xtics scale 0.5" set:"ytics scale 0.5" file:"burndown.plt" set:"xrange [0:17]"
#+BEGIN: block-update-burndown
#+END:
** TASK LIST
#+BEGIN: columnview :hlines 2 :maxlevel 5 :id "TASKS"
#+END:
* TASKS
  :PROPERTIES:
  :ID:       TASKS
  :SPRINTLENGTH: 13
  :SPRINTSTART: <2020-09-18 Fri>
  :wpd-akshay196: 1
  :wpd-bhavin192: 1
  :wpd-gandalfdwite: 1
  :wpd-jasonbraganza: 5
  :wpd-sandeepk: 1
  :END:
** akshay196
*** DONE The Go Programming Language - Part V
    CLOSED: [2020-09-28 Mon 23:55]
    :PROPERTIES:
    :ESTIMATED: 10
    :ACTUAL:   8.25
    :OWNER:    akshay196
    :ID:       READ.1587566538
    :TASKID:   READ.1587566538
    :END:
    :LOGBOOK:
    CLOCK: [2020-09-28 Mon 23:01]--[2020-09-28 Mon 23:55] =>  0:54
    CLOCK: [2020-09-28 Mon 21:51]--[2020-09-28 Mon 22:52] =>  1:01
    CLOCK: [2020-09-26 Sat 19:58]--[2020-09-26 Sat 21:01] =>  1:03
    CLOCK: [2020-09-25 Fri 20:05]--[2020-09-25 Fri 21:37] =>  1:32
    CLOCK: [2020-09-24 Thu 19:57]--[2020-09-24 Thu 21:00] =>  1:03
    CLOCK: [2020-09-23 Wed 20:04]--[2020-09-23 Wed 20:55] =>  0:51
    CLOCK: [2020-09-22 Tue 20:00]--[2020-09-22 Tue 21:05] =>  1:05
    CLOCK: [2020-09-19 Sat 23:24]--[2020-09-20 Sun 00:10] =>  0:46
    :END:
    https://learning.oreilly.com/library/view/the-go-programming/9780134190570/
    - [X] Chapter 12.7. Accessing Struct Field Tags                         (120m)
    - [X] Chapter 12.8. Displaying the Methods of a Type                    ( 30m)
    - [X] Chapter 12.9. A Word of Caution                                   ( 30m)
    - [X] Chapter 13.   Low-Level Programming                               ( 30m)
    - [X] Chapter 13.1. unsafe.Sizeof, Alignof, and Offsetof                ( 60m)
    - [X] Chapter 13.2. unsafe.Pointer                                      ( 60m)
    - [X] Chapter 13.3. Example: Deep Equivalence                           (120m)
    - [X] Chapter 13.4. Calling C Code with cgo                             (120m)
    - [X] Chapter 13.5. Another Word of Caution                             ( 30m)
*** DONE Go Tour excercise
    CLOSED: [2020-09-30 Wed 21:20]
    :PROPERTIES:
    :ESTIMATED: 3
    :ACTUAL:   4.52
    :OWNER: akshay196
    :ID: DEV.1600488136
    :TASKID: DEV.1600488136
    :END:
    :LOGBOOK:
    CLOCK: [2020-09-30 Wed 20:14]--[2020-09-30 Wed 21:20] =>  1:06
    CLOCK: [2020-09-29 Tue 08:05]--[2020-09-29 Tue 10:02] =>  1:57
    CLOCK: [2020-09-29 Tue 20:10]--[2020-09-29 Tue 21:38] =>  1:28
    :END:
    https://tour.golang.org/list

** bhavin192
*** DONE Site Reliability Engineering - III. Practices - Part II [1/1]
    CLOSED: [2020-09-28 Mon 20:55]
    :PROPERTIES:
    :ESTIMATED: 9
    :ACTUAL:   5.38
    :OWNER:    bhavin192
    :ID:       READ.1596455869
    :TASKID:   READ.1596455869
    :END:
    :LOGBOOK:
    CLOCK: [2020-09-28 Mon 20:15]--[2020-09-28 Mon 20:55] =>  0:40
    CLOCK: [2020-09-28 Mon 19:30]--[2020-09-28 Mon 19:48] =>  0:18
    CLOCK: [2020-09-27 Sun 22:40]--[2020-09-27 Sun 23:06] =>  0:26
    CLOCK: [2020-09-24 Thu 18:38]--[2020-09-24 Thu 19:38] =>  1:00
    CLOCK: [2020-09-23 Wed 21:00]--[2020-09-23 Wed 21:32] =>  0:32
    CLOCK: [2020-09-23 Wed 20:41]--[2020-09-23 Wed 20:54] =>  0:13
    CLOCK: [2020-09-22 Tue 22:20]--[2020-09-22 Tue 22:29] =>  0:09
    CLOCK: [2020-09-22 Tue 20:35]--[2020-09-22 Tue 21:20] =>  0:45
    CLOCK: [2020-09-21 Mon 20:41]--[2020-09-21 Mon 21:07] =>  0:26
    CLOCK: [2020-09-21 Mon 20:02]--[2020-09-21 Mon 20:36] =>  0:34
    CLOCK: [2020-09-18 Fri 20:40]--[2020-09-18 Fri 21:00] =>  0:20
    :END:
    https://landing.google.com/sre/sre-book/toc/index.html
    - [X] 12. Effective Troubleshooting                                            (6h)
*** DONE Attend PyCon India Flames - Python Mega Meetup
    CLOSED: [2020-09-19 Sat 14:18]
    :PROPERTIES:
    :ESTIMATED: 4
    :ACTUAL:   4.27
    :OWNER:    bhavin192
    :ID:       EVENT.1600353515
    :TASKID:   EVENT.1600353515
    :END:
    :LOGBOOK:
    CLOCK: [2020-09-19 Sat 10:02]--[2020-09-19 Sat 14:18] =>  4:16
    :END:
    https://www.meetup.com/PythonPune/events/273162438/

** gandalfdwite
*** DONE Git Pocket Guide [6/6]
    CLOSED: [2020-09-30 Wed 16:04]
    :PROPERTIES:
    :ESTIMATED: 13
    :ACTUAL:   13.07
    :OWNER: gandalfdwite
    :ID: READ.1593841309
    :TASKID: READ.1593841309
    :END:
    :LOGBOOK:
    CLOCK: [2020-09-30 Wed 14:57]--[2020-09-30 Wed 16:00] =>  1:03
    CLOCK: [2020-09-29 Tue 15:49]--[2020-09-29 Tue 17:10] =>  1:21
    CLOCK: [2020-09-28 Mon 21:05]--[2020-09-28 Mon 22:20] =>  1:15
    CLOCK: [2020-09-27 Sun 17:10]--[2020-09-27 Sun 18:40] =>  1:30
    CLOCK: [2020-09-26 Sat 10:30]--[2020-09-26 Sat 11:50] =>  1:20
    CLOCK: [2020-09-25 Fri 20:00]--[2020-09-25 Fri 21:00] =>  1:00
    CLOCK: [2020-09-23 Wed 22:00]--[2020-09-23 Wed 23:15] =>  1:15
    CLOCK: [2020-09-22 Tue 23:30]--[2020-09-23 Wed 00:35] =>  1:05
    CLOCK: [2020-09-20 Sun 11:35]--[2020-09-20 Sun 13:45] =>  2:10
    CLOCK: [2020-09-19 Sat 20:00]--[2020-09-19 Sat 21:05] =>  1:05
    :END:
    - [X] Tracking Other Repositories ( 3h )
    - [X] Merging                     ( 2h )
    - [X] Naming Commits              ( 2h )
    - [X] Viewing History             ( 2h )
    - [X] Editing History             ( 2h )
    - [X] Understanding patches       ( 2h )
** jasonbraganza
*** DONE BEGLA 135 - English In Daily Life - Part I [100%]
    CLOSED: [2020-09-23 Wed 08:35]
     :PROPERTIES:
     :ESTIMATED: 7
     :ACTUAL:   2.45
     :OWNER: jasonbraganza
     :ID:       READ.1600310906
     :TASKID: READ.1600310906
     :END:
     :LOGBOOK:
     CLOCK: [2020-09-23 Wed 07:05]--[2020-09-23 Wed 08:32] =>  1:27
     CLOCK: [2020-09-21 Mon 07:00]--[2020-09-21 Mon 07:38] =>  0:38
     CLOCK: [2020-09-18 Fri 09:29]--[2020-09-18 Fri 09:51] =>  0:22
     :END:
     - [X] Greetings and Goodbye (2020/09/14 - 2020/09/18)
     - [X] Unit 01 - Starting and Ending Conversations
     - [X] Unit 02 - Social Small Talk
     - [X] Unit 03 - Feelings
     - [X] Unit 04 - Non Verbal Communication: Body Language
     - [X] The World Around Us (2020/09/21 - 2020/09/25)
     - [X] Unit 05 - Family and Friends
*** DONE BEGC 131 - Individual & Society - Part I [100%]
    CLOSED: [2020-09-23 Wed 10:13]
    :PROPERTIES:
    :ESTIMATED: 8
    :ACTUAL:   2.28
    :OWNER: jasonbraganza
    :ID: READ.1600311245
    :TASKID: READ.1600311245
    :END:
    :LOGBOOK:
    CLOCK: [2020-09-23 Wed 08:40]--[2020-09-23 Wed 10:13] =>  1:33
    CLOCK: [2020-09-21 Mon 07:40]--[2020-09-21 Mon 08:19] =>  0:39
    CLOCK: [2020-09-18 Fri 09:52]--[2020-09-18 Fri 09:57] =>  0:05
    :END:
    - [X] Block 01 - The Environment (2020/10/05 - 2020/10/09)
    - [X] Unit 01 - Animal Rights
    - [X] Unit 02 - Human Environment - A Speech
    - [X] Unit 03 - From a Cocoon With Hope
    - [X] Unit 04 - Saving the Environment
    - [X] Block 02 - Travel and Tourism (2020/10/12 - 2020/10/16)
    - [X] Unit 05 - Planning a Holiday
    - [X] Unit 06 - Hotels and Restaurants
*** IN_PROGRESS Complete all of Reuven Lerner’s exercises courses + books - Part II [0%]
    :PROPERTIES:
    :ESTIMATED: 8
    :ACTUAL:   0.52
    :OWNER: jasonbraganza
    :ID: DEV.1600409922
    :TASKID: DEV.1600409922
    :END:
    :LOGBOOK:
    CLOCK: [2020-09-21 Mon 11:36]--[2020-09-21 Mon 12:03] =>  0:27
    CLOCK: [2020-09-21 Mon 10:45]--[2020-09-21 Mon 11:16] =>  0:31
    :END:
**** IN_PROGRESS iterators (Total 13) [2/13]
     - [X] Exercise 1
     - [X] Exercise 2
     - [ ] Exercise 3
     - [ ] Exercise 4
     - [ ] Exercise 5
     - [ ] Exercise 6
     - [ ] Exercise 7
     - [ ] Exercise 8
     - [ ] Exercise 9
     - [ ] Exercise 10
     - [ ] Exercise 11
     - [ ] Exercise 12
     - [ ] Exercise 13
*** TODO Reuven Lerner, Practice Makes Regex - Part I [0%]
    :PROPERTIES:
    :ESTIMATED: 1
    :ACTUAL:
    :OWNER: jasonbraganza
    :ID: DEV.1600410440
    :TASKID: DEV.1600410440
    :END:
    - [ ] EXERCISE 1 Find Matches
    - [ ] EXERCISE 2 Five-letter words
    - [ ] EXERCISE 3 Double “f” in the middle

** sandeepk
*** DONE Wagtail [100%]
   :PROPERTIES:
   :ESTIMATED: 13
   :ACTUAL:   7.42
   :OWNER: sandeepk
   :ID: DEV.1598776989
   :TASKID: DEV.1598776989
   :END:
   :LOGBOOK:
   CLOCK: [2020-09-30 Wed 08:30]--[2020-09-30 Wed 09:30] =>  1:00
   CLOCK: [2020-09-29 Tue 21:30]--[2020-09-29 Tue 22:20] =>  0:50
   CLOCK: [2020-09-28 Mon 20:00]--[2020-09-28 Mon 20:30] =>  0:30
   CLOCK: [2020-09-27 Sun 19:00]--[2020-09-27 Sun 19:30] =>  0:30
   CLOCK: [2020-09-24 Thu 09:00]--[2020-09-24 Thu 09:55] =>  0:55
   CLOCK: [2020-09-23 Wed 21:30]--[2020-09-23 Wed 22:20] =>  0:50
   CLOCK: [2020-09-21 Mon 08:30]--[2020-09-21 Mon 09:30] =>  1:00
   CLOCK: [2020-09-20 Sun 17:20]--[2020-09-20 Sun 18:35] =>  1:15
   CLOCK: [2020-09-18 Fri 21:05]--[2020-09-18 Fri 21:40] =>  0:35
   :END:
   - [X] Read the guidelines                                     ( 60m  )
   - [X] Explore around the project, find the issue to work on   ( 300m )
   - [X] Work on some patches                                    ( 420m )   

