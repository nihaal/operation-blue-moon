#+TITLE: March 9-24, 2020 (16 days)
#+PROPERTY: Effort_ALL 0 0:05 0:10 0:30 1:00 2:00 3:00 4:00
#+COLUMNS: %35ITEM %TASKID %OWNER %3PRIORITY %TODO %5ESTIMATED{+} %3ACTUAL{+}
* REPORTS
** SCRUM BOARD
#+BEGIN: block-update-board
#+END:
** DEVELOPER SUMMARY
#+BEGIN: block-update-summary
#+END:
** BURNDOWN CHART
#+BEGIN: block-update-graph
#+END:
** BURNDOWN LIST
#+PLOT: title:"Burndown" ind:1 deps:(3 4) set:"term dumb" set:"xtics scale 0.5" set:"ytics scale 0.5" file:"burndown.plt" set:"xrange [0:17]"
#+BEGIN: block-update-burndown
#+END:
** TASK LIST
#+BEGIN: columnview :hlines 2 :maxlevel 5 :id "TASKS"
#+END:
* TASKS
  :PROPERTIES:
  :ID:       TASKS
  :SPRINTLENGTH: 16
  :SPRINTSTART: <2020-03-09 Mon>
  :wpd-akshay196: 1
  :wpd-bhavin192: 1
  :wpd-gandalfdwite: 1
  :wpd-kurianbenoy: 1
  :END:
** akshay196
*** DONE Introduction to Operating Systems - Part III
    CLOSED: [2020-03-24 Tue 17:10]
    :PROPERTIES:
    :ESTIMATED: 16
    :ACTUAL:   12.37
    :OWNER: akshay196
    :ID: READ.1580485531
    :TASKID: READ.1580485531
    :END:
    :LOGBOOK:
    CLOCK: [2020-03-24 Tue 16:12]--[2020-03-24 Tue 17:10] =>  0:58
    CLOCK: [2020-03-24 Tue 06:23]--[2020-03-24 Tue 07:46] =>  1:23
    CLOCK: [2020-03-23 Mon 17:21]--[2020-03-23 Mon 18:35] =>  1:14
    CLOCK: [2020-03-23 Mon 08:40]--[2020-03-23 Mon 09:31] =>  0:51
    CLOCK: [2020-03-22 Sun 08:02]--[2020-03-22 Sun 08:55] =>  0:53
    CLOCK: [2020-03-21 Sat 06:11]--[2020-03-21 Sat 07:22] =>  1:11
    CLOCK: [2020-03-20 Fri 05:27]--[2020-03-20 Fri 07:02] =>  1:35
    CLOCK: [2020-03-19 Thu 07:47]--[2020-03-19 Thu 08:28] =>  0:41
    CLOCK: [2020-03-19 Thu 05:59]--[2020-03-19 Thu 06:12] =>  0:13
    CLOCK: [2020-03-18 Wed 05:28]--[2020-03-18 Wed 06:08] =>  0:40
    CLOCK: [2020-03-15 Sun 06:30]--[2020-03-15 Sun 07:20] =>  0:50
    CLOCK: [2020-03-14 Sat 21:16]--[2020-03-14 Sat 21:32] =>  0:16
    CLOCK: [2020-03-11 Wed 22:46]--[2020-03-11 Wed 23:07] =>  0:21
    CLOCK: [2020-03-11 Wed 05:55]--[2020-03-11 Wed 06:17] =>  0:22
    CLOCK: [2020-03-10 Tue 10:24]--[2020-03-10 Tue 11:18] =>  0:54
    :END:
    https://classroom.udacity.com/courses/ud923
    - [X] Inter-Process Communication           ( 90 min)
    - [X] Syncronization Constructs             (180 min)
    - [X] I/O Managements                       (120 min)
    - [X] Virtualization                        (120 min)
    - [X] Remote Procedure Calls                (180 min)
    - [X] Distributed File System               (120 min)
    - [X] Distributed Shared Memory             (180 min)

** bhavin192
*** DONE Work on ingress-nginx issue #5005
    CLOSED: [2020-03-22 Sun 23:53]
    :PROPERTIES:
    :ESTIMATED: 6
    :ACTUAL:   9.02
    :OWNER:    bhavin192
    :ID:       DEV.1584037043
    :TASKID:   DEV.1584037043
    :END:
    :LOGBOOK:
    CLOCK: [2020-03-22 Sun 23:17]--[2020-03-22 Sun 23:53] =>  0:36
    CLOCK: [2020-03-22 Sun 15:50]--[2020-03-22 Sun 16:11] =>  0:21
    CLOCK: [2020-03-22 Sun 00:14]--[2020-03-22 Sun 01:55] =>  1:41
    CLOCK: [2020-03-21 Sat 01:13]--[2020-03-21 Sat 01:26] =>  0:13
    CLOCK: [2020-03-19 Thu 23:09]--[2020-03-19 Thu 23:50] =>  0:41
    CLOCK: [2020-03-18 Wed 22:55]--[2020-03-18 Wed 23:10] =>  0:15
    CLOCK: [2020-03-18 Wed 22:09]--[2020-03-18 Wed 22:36] =>  0:27
    CLOCK: [2020-03-17 Tue 22:13]--[2020-03-17 Tue 22:31] =>  0:18
    CLOCK: [2020-03-17 Tue 21:28]--[2020-03-17 Tue 21:33] =>  0:05
    CLOCK: [2020-03-17 Tue 19:07]--[2020-03-17 Tue 20:32] =>  1:25
    CLOCK: [2020-03-15 Sun 16:40]--[2020-03-15 Sun 17:53] =>  1:13
    CLOCK: [2020-03-15 Sun 13:35]--[2020-03-15 Sun 14:31] =>  0:56
    CLOCK: [2020-03-13 Fri 19:47]--[2020-03-13 Fri 20:37] =>  0:50
    :END:
    https://github.com/kubernetes/ingress-nginx/issues/5005
*** DONE Watch FOSDEM 2020 videos - Part I [18/18]
    CLOSED: [2020-03-24 Tue 23:55]
    :PROPERTIES:
    :ESTIMATED: 10
    :ACTUAL:   8.77
    :OWNER:    bhavin192
    :ID:       READ.1584036845
    :TASKID:   READ.1584036845
    :END:
    :LOGBOOK:
    CLOCK: [2020-03-24 Tue 21:41]--[2020-03-24 Tue 23:55] =>  2:14
    CLOCK: [2020-03-24 Tue 19:47]--[2020-03-24 Tue 21:10] =>  1:23
    CLOCK: [2020-03-23 Mon 21:55]--[2020-03-23 Mon 22:20] =>  0:25
    CLOCK: [2020-03-23 Mon 20:02]--[2020-03-23 Mon 21:18] =>  1:16
    CLOCK: [2020-03-22 Sun 21:56]--[2020-03-22 Sun 23:11] =>  1:15
    CLOCK: [2020-03-22 Sun 21:05]--[2020-03-22 Sun 21:28] =>  0:23
    CLOCK: [2020-03-22 Sun 18:21]--[2020-03-22 Sun 20:11] =>  1:50
    :END:
    - [X] [[https://fosdem.org/2020/schedule/event/fundamental_technologies_we_need_to_work_on_for_cloud_native_networking/][Fundamental Technologies We Need to Work on for Cloud-Native Networking]]        (50m)
    - [X] [[https://fosdem.org/2020/schedule/event/tanka/][Introducing Tanka]]                                                              (30m)
    - [X] [[https://fosdem.org/2020/schedule/event/beam_opentelemetry_xkcd_927_success_story/][OpenTelemetry: an XKCD 927 Success Story]]                                       (25m)
    - [X] [[https://fosdem.org/2020/schedule/event/containers_live_migration/][Container Live Migration ]]                                                      (25m)
    - [X] [[https://fosdem.org/2020/schedule/event/containers_syscall_emulation/][Supervising and emulating syscalls]]                                             (25m)
    - [X] [[https://fosdem.org/2020/schedule/event/containers_k8s_runtimes/][Below Kubernetes: Demystifying container runtimes]]                              (25m)
    - [X] [[https://fosdem.org/2020/schedule/event/riek_kubernetes/][How Containers and Kubernetes re-defined the GNU/Linux Operating System]]        (25m)
    - [X] [[https://fosdem.org/2020/schedule/event/codeworkload/][Code Workload Management into the Control Plane]]                                (30m)
    - [X] [[https://fosdem.org/2020/schedule/event/containers_memory_management/][Linux memory management at scale]]                                               (35m)
    - [X] [[https://fosdem.org/2020/schedule/event/mysql_k8s/][Running MySQL in Kubernetes in real life]]                                       (25m)
    - [X] [[https://fosdem.org/2020/schedule/event/security_secure_logging_with_syslog_ng/][Secure logging with syslog-ng]]                                                  (30m)
    - [X] [[https://fosdem.org/2020/schedule/event/containers_lxd/][Running full Linux systems in containers, at scale]]                             (25m)
    - [X] [[https://fosdem.org/2020/schedule/event/kubernetes/][Fixing the Kubernetes clusterfuck]]                                              (60m)
    - [X] [[https://fosdem.org/2020/schedule/event/security_protecting_plaintext_secrets_in_configuration_files/][Protecting plaintext secrets in configuration files]]                            (30m)
    - [X] [[https://fosdem.org/2020/schedule/event/containers_k8s_security/][How (Not) To Containerise Securely]]                                             (35m)
    - [X] [[https://fosdem.org/2020/schedule/event/terraform/][Hacking Terraform for fun and profit]]                                           (30m)
    - [X] [[https://fosdem.org/2020/schedule/event/containers_k8s_crio_lxc/][Using crio-lxc with Kubernetes]]                                                 (35m)
    - [X] [[https://fosdem.org/2020/schedule/event/http3/][HTTP/3 for everyone]]                                                            (60m)

** gandalfdwite
*** DONE Learning Terraform [4/4]
    CLOSED: [2020-03-27 Fri 08:56]
    :PROPERTIES:
    :ESTIMATED: 12
    :ACTUAL:   12.68
    :OWNER: gandalfdwite
    :ID: OPS.1563198652
    :TASKID: OPS.1563198652
    :END:
    :LOGBOOK:
    CLOCK: [2020-03-20 Fri 16:00]--[2020-03-20 Fri 17:05] =>  1:05
    CLOCK: [2020-03-19 Thu 23:50]--[2020-03-20 Fri 01:05] =>  1:15
    CLOCK: [2020-03-18 Wed 22:35]--[2020-03-18 Wed 23:45] =>  1:10
    CLOCK: [2020-03-17 Tue 20:30]--[2020-03-17 Tue 21:40] =>  1:10
    CLOCK: [2020-03-16 Mon 21:00]--[2020-03-16 Mon 22:05] =>  1:05
    CLOCK: [2020-03-15 Sun 10:20]--[2020-03-15 Sun 11:40] =>  1:20
    CLOCK: [2020-03-14 Sat 14:50]--[2020-03-14 Sat 16:25] =>  1:35
    CLOCK: [2020-03-13 Fri 21:00]--[2020-03-13 Fri 22:15] =>  1:15
    CLOCK: [2020-03-12 Thu 18:05]--[2020-03-12 Thu 19:50] =>  1:45
    CLOCK: [2020-03-11 Wed 23:49]--[2020-03-12 Thu 00:50] =>  1:01
    :END:
    - [X] Introduction To Terraform   ( 2h )
    - [X] Terraform CLI               ( 4h )
    - [X] Terraform With AWS          ( 3h )
    - [X] Terraform With Azure        ( 3h )
*** DONE Learn OCaml for Emacs Bug Triaging [2/2]
    CLOSED: [2020-03-27 Fri 10:24]
    :PROPERTIES:
    :ESTIMATED: 4
    :ACTUAL:   4.40
    :OWNER: gandalfdwite
    :ID: READ.1580178290
    :TASKID: READ.1580178290
    :END:
    :LOGBOOK
    CLOCK: [2020-03-24 Tue 17:00]--[2020-03-24 Tue 18:23] =>  1:23
    CLOCK: [2020-03-22 Sun 13:10]--[2020-03-22 Sun 14:23] =>  1:13
    CLOCK: [2020-03-21 Sat 10:35]--[2020-03-21 Sat 12:23] =>  1:48
    :END:
    - [X] Imperative Programming        ( 2h )
    - [X] A complete Programme          ( 2h )
** kurianbenoy
*** Swift in Depth book - Part 1
   :PROPERTIES:
   :ESTIMATED: 7
   :ACTUAL:
   :OWNER: kurianbenoy
   :ID: READ.1583941789
   :TASKID: READ.1583941789
   :END:
   :LOGBOOK:
   CLOCK: [2020-03-16 Mon 14:54]--[2020-03-16 Mon 15:16] =>  0:22
   CLOCK: [2020-03-14 Sat 23:13]--[2020-03-15 Sun 00:06] =>  0:53
   CLOCK: [2020-03-14 Sat 21:28]--[2020-03-14 Sat 23:00] =>  1:32
   CLOCK: [2020-03-13 Fri 20:45]--[2020-03-13 Fri 22:08] =>  1:23
   :END:
   - [X] Hello World guide to swift
   - [ ] Modelling Data with Enums
   - [ ] Writing Cleaner Properties
*** Fluent Python
  :PROPERTIES:
   :ESTIMATED: 4
   :ACTUAL:
   :OWNER: kurianbenoy
   :ID: READ.1583941893
   :TASKID: READ.1583941893
   :END: 
  :LOGBOOK:
  CLOCK: [2020-03-15 Sun 22:37]--[2020-03-15 Sun 22:46] =>  0:09
  CLOCK: [2020-03-15 Sun 21:48]--[2020-03-15 Sun 22:19] =>  0:31
  CLOCK: [2020-03-13 Fri 00:24]--[2020-03-13 Fri 00:50] =>  0:26
  :END:
   - [X] Python Data Model
   - [ ] An array of sequences
*** Learn Emacs and modifying configs
   :PROPERTIES:
   :ESTIMATED: 5
   :ACTUAL:
   :OWNER: kurianbenoy
   :ID: DEV.1583941968
   :TASKID: DEV.1583941968
   :END:
   - [ ] Install Magit, watch and understand functionality
   - [ ] Modify Emacs config files
   - [ ] Python autocomplete IDE with emacs
