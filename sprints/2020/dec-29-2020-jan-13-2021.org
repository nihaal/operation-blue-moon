#+TITLE: December 29, 2020 - January 13, 2021 (16 days)
#+PROPERTY: Effort_ALL 0 0:05 0:10 0:30 1:00 2:00 3:00 4:00
#+COLUMNS: %35ITEM %TASKID %OWNER %3PRIORITY %TODO %5ESTIMATED{+} %3ACTUAL{+}
* REPORTS
** SCRUM BOARD
#+BEGIN: block-update-board
#+END:
** DEVELOPER SUMMARY
#+BEGIN: block-update-summary
#+END:
** BURNDOWN CHART
#+BEGIN: block-update-graph
#+END:
** BURNDOWN LIST
#+PLOT: title:"Burndown" ind:1 deps:(3 4) set:"term dumb" set:"xtics scale 0.5" set:"ytics scale 0.5" file:"burndown.plt" set:"xrange [0:17]"
#+BEGIN: block-update-burndown
#+END:
** TASK LIST
#+BEGIN: columnview :hlines 2 :maxlevel 5 :id "TASKS"
#+END:
* TASKS
  :PROPERTIES:
  :ID:       TASKS
  :SPRINTLENGTH: 16
  :SPRINTSTART: <2020-12-29 Tue>
  :wpd-akshay196: 1
  :wpd-bhavin192: 1
  :END:
** akshay196
*** DONE Solve Go Examples - Part I [6/6]
    CLOSED: [2021-01-13 Wed 22:12]
    :PROPERTIES:
    :ESTIMATED: 16
    :ACTUAL:   9.83
    :OWNER: akshay196
    :ID: DEV.1609175417
    :TASKID: DEV.1609175417
    :END:
    :LOGBOOK:
    CLOCK: [2021-01-13 Wed 19:08]--[2021-01-13 Wed 22:12] =>  3:04
    CLOCK: [2021-01-12 Tue 22:30]--[2021-01-12 Tue 23:30] =>  1:00
    CLOCK: [2021-01-11 Mon 21:19]--[2021-01-11 Mon 22:03] =>  0:44
    CLOCK: [2021-01-09 Sat 19:11]--[2021-01-09 Sat 21:22] =>  2:11
    CLOCK: [2021-01-06 Wed 23:04]--[2021-01-06 Wed 23:49] =>  0:45
    CLOCK: [2021-01-01 Fri 12:53]--[2021-01-01 Fri 14:40] =>  1:47
    CLOCK: [2020-12-30 Wed 01:27]--[2020-12-30 Wed 01:46] =>  0:19
    :END:
     https://github.com/inancgumus/learngo
    - [X] 01-get-started                              (1h)
    - [X] 02-write-your-first-program                 (1h)
    - [X] 03-packages-and-scopes                      (1h)
    - [X] 04-statements-expressions-comments          (1h)
    - [X] 05-write-your-first-library-package         (2h)
    - [X] 06-variables                                (2h)
** bhavin192
*** DONE Site Reliability Engineering - III. Practices - Part VIII [1/1]
    CLOSED: [2021-01-06 Wed 21:10]
    :PROPERTIES:
    :ESTIMATED: 16
    :ACTUAL:   7.50
    :OWNER:    bhavin192
    :ID:       READ.1596455869
    :TASKID:   READ.1596455869
    :END:
    :LOGBOOK:
    CLOCK: [2021-01-06 Wed 20:53]--[2021-01-06 Wed 21:10] =>  0:17
    CLOCK: [2021-01-06 Wed 20:44]--[2021-01-06 Wed 20:53] =>  0:09
    CLOCK: [2021-01-06 Wed 20:08]--[2021-01-06 Wed 20:34] =>  0:26
    CLOCK: [2021-01-05 Tue 20:07]--[2021-01-05 Tue 21:05] =>  0:58
    CLOCK: [2021-01-04 Mon 22:11]--[2021-01-04 Mon 22:22] =>  0:11
    CLOCK: [2021-01-04 Mon 21:47]--[2021-01-04 Mon 22:11] =>  0:24
    CLOCK: [2021-01-04 Mon 20:24]--[2021-01-04 Mon 20:44] =>  0:20
    CLOCK: [2021-01-03 Sun 17:32]--[2021-01-03 Sun 17:39] =>  0:07
    CLOCK: [2021-01-03 Sun 17:07]--[2021-01-03 Sun 17:31] =>  0:24
    CLOCK: [2021-01-03 Sun 16:41]--[2021-01-03 Sun 17:07] =>  0:26
    CLOCK: [2021-01-02 Sat 15:41]--[2021-01-02 Sat 15:58] =>  0:17
    CLOCK: [2021-01-02 Sat 13:25]--[2021-01-02 Sat 13:46] =>  0:21
    CLOCK: [2021-01-02 Sat 12:53]--[2021-01-02 Sat 13:14] =>  0:21
    CLOCK: [2021-01-01 Fri 20:21]--[2021-01-01 Fri 20:48] =>  0:27
    CLOCK: [2021-01-01 Fri 19:07]--[2021-01-01 Fri 19:43] =>  0:36
    CLOCK: [2021-01-01 Fri 18:36]--[2021-01-01 Fri 18:46] =>  0:10
    CLOCK: [2020-12-31 Thu 22:48]--[2020-12-31 Thu 23:21] =>  0:33
    CLOCK: [2020-12-30 Wed 20:48]--[2020-12-30 Wed 21:06] =>  0:18
    CLOCK: [2020-12-29 Tue 20:46]--[2020-12-29 Tue 21:31] =>  0:45
    :END:
    https://landing.google.com/sre/sre-book/toc/index.html
    - [X] 22. Addressing Cascading Failures                                             (8h)
