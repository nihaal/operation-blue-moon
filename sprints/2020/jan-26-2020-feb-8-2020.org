#+TITLE: January 26-February 8, 2020 (14 days)
#+PROPERTY: Effort_ALL 0 0:05 0:10 0:30 1:00 2:00 3:00 4:00
#+COLUMNS: %35ITEM %TASKID %OWNER %3PRIORITY %TODO %5ESTIMATED{+} %3ACTUAL{+}
* REPORTS
** SCRUM BOARD
#+BEGIN: block-update-board
#+END:
** DEVELOPER SUMMARY
#+BEGIN: block-update-summary
#+END:
** BURNDOWN CHART
#+BEGIN: block-update-graph
#+END:
** BURNDOWN LIST
#+PLOT: title:"Burndown" ind:1 deps:(3 4) set:"term dumb" set:"xtics scale 0.5" set:"ytics scale 0.5" file:"burndown.plt" set:"xrange [0:17]"
#+BEGIN: block-update-burndown
#+END:
** TASK LIST
#+BEGIN: columnview :hlines 2 :maxlevel 5 :id "TASKS"
#+END:
* TASKS
  :PROPERTIES:
  :ID:       TASKS
  :SPRINTLENGTH: 14
  :SPRINTSTART: <2020-01-26 Sun>
  :wpd-akshay196: 2
  :wpd-gandalfdwite: 1
  :wpd-kurianbenoy: 3
  :END:
** akshay196
*** DONE Red Hat Certified System Administrator (RHCSA) Training [15/15]
    CLOSED: [2020-02-02 Sun 16:48]
    :PROPERTIES:
    :ESTIMATED: 15
    :ACTUAL:   13.07
    :OWNER: akshay196
    :ID: READ.1579937417
    :TASKID: READ.1579937417
    :END:
    :LOGBOOK:
    CLOCK: [2020-02-02 Sun 16:29]--[2020-02-02 Sun 16:48] =>  0:19
    CLOCK: [2020-02-02 Sun 15:34]--[2020-02-02 Sun 16:09] =>  0:35
    CLOCK: [2020-02-02 Sun 07:57]--[2020-02-02 Sun 08:36] =>  0:39
    CLOCK: [2020-02-01 Sat 17:24]--[2020-02-01 Sat 18:45] =>  1:21
    CLOCK: [2020-01-31 Fri 22:24]--[2020-01-31 Fri 22:49] =>  0:25
    CLOCK: [2020-01-31 Fri 07:43]--[2020-01-31 Fri 09:06] =>  1:23
    CLOCK: [2020-01-30 Thu 22:52]--[2020-01-31 Fri 00:21] =>  1:29
    CLOCK: [2020-01-30 Thu 07:47]--[2020-01-30 Thu 09:00] =>  1:13
    CLOCK: [2020-01-29 Wed 22:31]--[2020-01-29 Wed 22:50] =>  0:19
    CLOCK: [2020-01-29 Wed 07:05]--[2020-01-29 Wed 08:06] =>  1:01
    CLOCK: [2020-01-28 Tue 23:22]--[2020-01-28 Tue 23:43] =>  0:21
    CLOCK: [2020-01-28 Tue 21:57]--[2020-01-28 Tue 23:07] =>  1:10
    CLOCK: [2020-01-28 Tue 06:40]--[2020-01-28 Tue 07:37] =>  0:57
    CLOCK: [2020-01-27 Mon 20:30]--[2020-01-27 Mon 21:05] =>  0:35
    CLOCK: [2020-01-27 Mon 07:17]--[2020-01-27 Mon 08:34] =>  1:17
    :END:
    - [X] Chapter  1
    - [X] Chapter  2
    - [X] Chapter  3
    - [X] Chapter  4
    - [X] Chapter  5
    - [X] Chapter  6
    - [X] Chapter  7
    - [X] Chapter  8
    - [X] Chapter  9
    - [X] Chapter 10
    - [X] Chapter 11
    - [X] Chapter 12
    - [X] Chapter 13
    - [X] Chapter 14
    - [X] Chapter 15
*** DONE Red Hat Certified Engineer (RHCE) Training [13/13]
    CLOSED: [2020-02-08 Sat 23:55]
    :PROPERTIES:
    :ESTIMATED: 13
    :ACTUAL:   10.05
    :OWNER: akshay196
    :ID: READ.1579937451
    :TASKID: READ.1579937451
    :END:
    :LOGBOOK:
    CLOCK: [2020-02-08 Sat 20:55]--[2020-02-08 Sat 23:55] =>  3:00
    CLOCK: [2020-02-07 Fri 17:40]--[2020-02-07 Fri 19:00] =>  1:20
    CLOCK: [2020-02-07 Fri 07:53]--[2020-02-07 Fri 09:50] =>  1:57
    CLOCK: [2020-02-06 Thu 07:23]--[2020-02-06 Thu 08:04] =>  0:41
    CLOCK: [2020-02-05 Wed 06:59]--[2020-02-05 Wed 08:20] =>  1:21
    CLOCK: [2020-02-04 Tue 07:11]--[2020-02-04 Tue 07:41] =>  0:30
    CLOCK: [2020-02-03 Mon 21:25]--[2020-02-03 Mon 21:51] =>  0:26
    CLOCK: [2020-02-03 Mon 20:24]--[2020-02-03 Mon 21:12] =>  0:48
    :END:
    - [X] Chapter  1
    - [X] Chapter  2
    - [X] Chapter  3
    - [X] Chapter  4
    - [X] Chapter  5
    - [X] Chapter  6
    - [X] Chapter  7
    - [X] Chapter  8
    - [X] Chapter  9
    - [X] Chapter 10
    - [X] Chapter 11
    - [X] Chapter 12
    - [X] Chapter 13
** gandalfdwite
*** DONE Learn OCaml for Emacs Bug Triaging Part I - [6/6]
    CLOSED: [2020-03-12 Thu 20:49]
    :PROPERTIES:
    :ESTIMATED: 10
    :ACTUAL:   10.35
    :OWNER: gandalfdwite
    :ID: READ.1580178290
    :TASKID: READ.1580178290
    :END:
    :LOGBOOK:
    CLOCK: [2020-02-07 Fri 21:55]--[2020-02-07 Fri 23:20] =>  1:25
    CLOCK: [2020-02-06 Thu 18:55]--[2020-02-06 Thu 20:01] =>  1:06
    CLOCK: [2020-02-05 Wed 23:14]--[2020-02-06 Thu 00:05] =>  0:51
    CLOCK: [2020-02-04 Tue 19:21]--[2020-02-04 Tue 20:59] =>  1:38
    CLOCK: [2020-02-03 Mon 10:48]--[2020-02-03 Mon 12:05] =>  1:17
    CLOCK: [2020-02-02 Sun 15:39]--[2020-02-02 Sun 17:15] =>  1:36
    CLOCK: [2020-01-31 Fri 14:04]--[2020-01-31 Fri 15:15] =>  1:11
    CLOCK: [2020-01-28 Tue 23:18]--[2020-01-29 Wed 00:35] =>  1:17
    :END:
    - [X] Setup OCaml and opam          ( 1h )
    - [X] Base, Core and Core_Kernel    ( 2h )
    - [X] OCaml as a calculator         ( 2h )
    - [X] Functions and Type Inference  ( 2h )
    - [X] Tuples, List, Options and     ( 2h )
          Pattern matching
    - [X] Records and Variants          ( 1h )
*** DONE Blog post on containerizing pravarag dot com [1/1]
    CLOSED: [2020-03-12 Thu 20:50]
    :PROPERTIES:
    :ESTIMATED: 4
    :ACTUAL:   4.02
    :OWNER: gandalfdwite
    :ID: WRITE.1580179018
    :TASKID: WRITE.1580179018
    :END:
    :LOGBOOK:
    CLOCK: [2020-01-30 Thu 20:00]--[2020-01-30 Thu 21:01] =>  1:01
    CLOCK: [2020-01-27 Mon 21:15]--[2020-01-27 Mon 22:35] =>  1:20
    CLOCK: [2020-01-26 Sun 12:30]--[2020-01-26 Sun 14:10] =>  1:40
    :END:
    - [X] Blog post on Containerizing pravarag dot com   ( 4h )
** kurianbenoy
*** DONE Malayalam TTS Project - Part 2
   :PROPERTIES:
   :ESTIMATED: 14
   :ACTUAL: 4.62
   :OWNER: kurianbenoy
   :ID: DEV.1580731550
   :TASKID: DEV.1580731550
   :END:
   :LOGBOOK:
   CLOCK: [2020-02-09 Sun 19:55]--[2020-02-09 Sun 21:14] =>  1:19
   CLOCK: [2020-02-07 Fri 14:00]--[2020-02-07 Fri 14:50] =>  0:50 
   CLOCK: [2020-02-07 Fri 11:28]--[2020-02-07 Fri 12:45] =>  1:17
   CLOCK: [2020-02-07 Fri 09:17]--[2020-02-07 Fri 10:31] =>  1:14
   :END:
*** DONE Kaggle Bengali CV competition
   :PROPERTIES:
   :ESTIMATED: 10
   :ACTUAL: 3.233
   :OWNER: kurianbenoy
   :ID: DEV.1580731595
   :TASKID: DEV.1580731595
   :END:
   :LOGBOOK:
   CLOCK: [2020-02-09 Sun 21:14]--[2020-02-10 Mon 00:28] =>  3:14
   :END:

