#+TITLE: June 5-21, 2020 (17 days)
#+PROPERTY: Effort_ALL 0 0:05 0:10 0:30 1:00 2:00 3:00 4:00
#+COLUMNS: %35ITEM %TASKID %OWNER %3PRIORITY %TODO %5ESTIMATED{+} %3ACTUAL{+}
* REPORTS
** SCRUM BOARD
#+BEGIN: block-update-board
#+END:
** DEVELOPER SUMMARY
#+BEGIN: block-update-summary
#+END:
** BURNDOWN CHART
#+BEGIN: block-update-graph
#+END:
** BURNDOWN LIST
#+PLOT: title:"Burndown" ind:1 deps:(3 4) set:"term dumb" set:"xtics scale 0.5" set:"ytics scale 0.5" file:"burndown.plt" set:"xrange [0:17]"
#+BEGIN: block-update-burndown
#+END:
** TASK LIST
#+BEGIN: columnview :hlines 2 :maxlevel 5 :id "TASKS"
#+END:
* TASKS
  :PROPERTIES:
  :ID:       TASKS
  :SPRINTLENGTH: 17
  :SPRINTSTART: <2020-06-05 Fri>
  :wpd-akshay196: 1
  :wpd-bhavin192: 1
  :wpd-sandeepk: 1.1
  :END:
** akshay196
*** DONE The Go Programming Language - Part III [16/16]
    CLOSED: [2020-06-21 Sun 10:34]
    :PROPERTIES:
    :ESTIMATED: 16
    :ACTUAL:   16.98
    :OWNER:    akshay196
    :ID:       READ.1587566538
    :TASKID:   READ.1587566538
    :END:
    :LOGBOOK:
    CLOCK: [2020-06-21 Sun 08:20]--[2020-06-21 Sun 10:34] =>  2:14
    CLOCK: [2020-06-20 Sat 19:25]--[2020-06-20 Sat 20:46] =>  1:21
    CLOCK: [2020-06-20 Sat 14:14]--[2020-06-20 Sat 15:53] =>  1:39
    CLOCK: [2020-06-19 Fri 17:04]--[2020-06-19 Fri 18:21] =>  1:17
    CLOCK: [2020-06-19 Fri 08:35]--[2020-06-19 Fri 10:54] =>  2:19
    CLOCK: [2020-06-18 Thu 18:29]--[2020-06-18 Thu 19:15] =>  0:46
    CLOCK: [2020-06-18 Thu 09:04]--[2020-06-18 Thu 10:30] =>  1:26
    CLOCK: [2020-06-17 Wed 08:14]--[2020-06-17 Wed 09:05] =>  0:51
    CLOCK: [2020-06-16 Tue 08:32]--[2020-06-16 Tue 08:54] =>  0:22
    CLOCK: [2020-06-15 Mon 15:17]--[2020-06-15 Mon 15:54] =>  0:37
    CLOCK: [2020-06-15 Mon 12:36]--[2020-06-15 Mon 13:09] =>  0:33
    CLOCK: [2020-06-15 Mon 08:09]--[2020-06-15 Mon 08:44] =>  0:35
    CLOCK: [2020-06-12 Fri 07:58]--[2020-06-12 Fri 08:34] =>  0:36
    CLOCK: [2020-06-11 Thu 06:43]--[2020-06-11 Thu 07:51] =>  1:08
    CLOCK: [2020-06-09 Tue 22:03]--[2020-06-09 Tue 22:56] =>  0:53
    CLOCK: [2020-06-09 Tue 02:14]--[2020-06-09 Tue 02:36] =>  0:22
    :END:
    https://learning.oreilly.com/library/view/the-go-programming/9780134190570/
    - [X] Chapter 7.5. Interface Values                                     ( 60m)
    - [X] Chapter 7.6. Sorting with sort.Interface                          (120m)
    - [X] Chapter 7.7. The http.Handler Interface                           (120m)
    - [X] Chapter 7.8. The error Interface                                  ( 60m)
    - [X] Chapter 7.9. Example: Expression Evaluator                        (120m)
    - [X] Chapter 7.10. Type Assertions                                     ( 60m)
    - [X] Chapter 7.11. Discriminating Errors with Type Assertions          ( 30m)
    - [X] Chapter 7.12. Querying Behaviors with Interface Type Assertions   ( 60m)
    - [X] Chapter 7.13. Type Switches                                       ( 60m)
    - [X] Chapter 7.14. Example: Token-Based XML Decoding                   ( 90m)
    - [X] Chapter 7.15. A Few Words of Advice                               ( 15m)
    - [X] Chapter 8.   Goroutines and Channels                              ( 15m)
    - [X] Chapter 8.1. Goroutines                                           ( 30m)
    - [X] Chapter 8.2. Example: Concurrent Clock Server                     ( 60m)
    - [X] Chapter 8.3. Example: Concurrent Echo Server                      ( 60m)
    - [X] Chapter 8.4. Channels                                             (120m)

** bhavin192
*** DONE Programming Kubernetes - 4. Using Custom Resources
    CLOSED: [2020-06-14 Sun 19:51]
    :PROPERTIES:
    :ESTIMATED: 6
    :ACTUAL:   5.33
    :OWNER:    bhavin192
    :ID:       READ.1588871088
    :TASKID:   READ.1588871088
    :END:
    :LOGBOOK:
    CLOCK: [2020-06-14 Sun 19:26]--[2020-06-14 Sun 19:51] =>  0:25
    CLOCK: [2020-06-14 Sun 18:54]--[2020-06-14 Sun 19:16] =>  0:22
    CLOCK: [2020-06-14 Sun 17:41]--[2020-06-14 Sun 18:46] =>  1:05
    CLOCK: [2020-06-10 Wed 21:51]--[2020-06-10 Wed 22:14] =>  0:23
    CLOCK: [2020-06-10 Wed 20:16]--[2020-06-10 Wed 21:00] =>  0:44
    CLOCK: [2020-06-08 Mon 19:45]--[2020-06-08 Mon 20:50] =>  1:05
    CLOCK: [2020-06-07 Sun 22:12]--[2020-06-07 Sun 22:34] =>  0:22
    CLOCK: [2020-06-07 Sun 20:52]--[2020-06-07 Sun 21:05] =>  0:13
    CLOCK: [2020-06-07 Sun 20:32]--[2020-06-07 Sun 20:48] =>  0:16
    CLOCK: [2020-06-07 Sun 20:03]--[2020-06-07 Sun 20:28] =>  0:25
    :END:
    https://learning.oreilly.com/library/view/programming-kubernetes/9781492047094/
*** DONE Programming Kubernetes - 5. Automating Code Generation
    CLOSED: [2020-06-17 Wed 23:34]
    :PROPERTIES:
    :ESTIMATED: 6
    :ACTUAL:   2.85
    :OWNER:    bhavin192
    :ID:       READ.1588871088
    :TASKID:   READ.1588871088
    :END:
    :LOGBOOK:
    CLOCK: [2020-06-17 Wed 22:28]--[2020-06-17 Wed 23:34] =>  1:06
    CLOCK: [2020-06-16 Tue 21:44]--[2020-06-16 Tue 21:55] =>  0:11
    CLOCK: [2020-06-15 Mon 22:22]--[2020-06-15 Mon 23:29] =>  1:07
    CLOCK: [2020-06-14 Sun 19:51]--[2020-06-14 Sun 20:18] =>  0:27
    :END:
    https://learning.oreilly.com/library/view/programming-kubernetes/9781492047094/
*** DONE Programming Kubernetes - 6. Solutions for Writing Operators
    CLOSED: [2020-06-20 Sat 21:25]
    :PROPERTIES:
    :ESTIMATED: 5
    :ACTUAL:   1.92
    :OWNER:    bhavin192
    :ID:       READ.1588871088
    :TASKID:   READ.1588871088
    :END:
    :LOGBOOK:
    CLOCK: [2020-06-20 Sat 20:30]--[2020-06-20 Sat 21:25] =>  0:55
    CLOCK: [2020-06-20 Sat 19:43]--[2020-06-20 Sat 20:18] =>  0:35
    CLOCK: [2020-06-19 Fri 22:51]--[2020-06-19 Fri 23:02] =>  0:11
    CLOCK: [2020-06-18 Thu 23:50]--[2020-06-19 Fri 00:04] =>  0:14
    :END:
    https://learning.oreilly.com/library/view/programming-kubernetes/9781492047094/

** sandeepk
*** DONE Book Fluent Python Part V [1/1]
    CLOSED: [2020-06-11 Thu 08:20] 
    :PROPERTIES:
    :ESTIMATED: 3
    :ACTUAL:   2.25
    :OWNER: sandeepk
    :ID: READ.1585286321
    :TASKID: READ.1585286321
    :END:
    :LOGBOOK:
    CLOCK: [2020-06-11 Thu 07:30]--[2020-06-11 Thu 08:20] =>  0:50
    CLOCK: [2020-06-08 Mon 08:30]--[2020-06-08 Mon 09:25] =>  0:55
    CLOCK: [2020-06-06 Sat 21:30]--[2020-06-06 Sat 22:00] =>  0:30
    :END:
    - [X] Chapter 17. Concurrency with Futures     ( 3 hr )

*** DONE O'REILLY Live Trainings [3/3]
   CLOSED: [2020-06-19 Fri 00:30]
   :PROPERTIES:
   :ESTIMATED: 10
   :ACTUAL:   7.00
   :OWNER: sandeepk
   :ID: READ.1591226667
   :TASKID: READ.1591226667
   :END:
   :LOGBOOOK:
   CLOCK: [2020-06-18 Thu 23:30]--[2020-06-19 Fri 00:30] =>  1:00
   CLOCK: [2020-06-17 Wed 21:30]--[2020-06-18 Thu 00:00] =>  2:30
   CLOCK: [2020-06-10 Wed 21:30]--[2020-06-11 Thu 01:00] =>  3:30
   :END:
   - [X] Software Architecture Superstream Series: Software Architecture Fundamentals    ( 4 hr )
   - [X] O'Reilly Infrastructure & Ops Superstream: SRE Edition                          ( 4 hr )
   - [X] OSCON Open Source Software Superstream Series: Live Coding Go, Rust, and Python ( 2 hr )

