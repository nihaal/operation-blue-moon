#+AUTHOR: Sayan Chowdhury
#+EMAIL: namoskar@yudocaa.in
#+TAGS: read write dev ops event meeting # Need to be category
#+STARTUP: hideblocks

----

* GOALS
** Computer Science
*** Introduction to Computer Systems [0/40]
    :PROPERTIES:
    :ESTIMATED: 72
    :ACTUAL:
    :OWNER:    yudocaa
    :ID:       READ.1615951521
    :TASKID:   READ.1615951521
    :END:
    - [ ] Lecture 02: Bits, Bytes and Integers                            (2h)
    - [ ] Lecture 03: Bits, Bytes, and Integers (Contd.)                  (2h)
    - [ ] Lecture 04: Floating Point                                      (2h)
    - [ ] Recitation 3: Datalab and Data Representations                  (1.5h)
    - [ ] Lecture 05: Machine Level Programming I: Basics                 (2h)
    - [ ] Lecture 06: Machine Level Programming II: Control               (2h)
    - [ ] Recitation 4: Bomb Lab                                          (1.5h)
    - [ ] Lecture 07: Machine Level Programming III: Procedures           (2h)
    - [ ] Lecture 08: Machine Level Programming IV: Data                  (2h)
    - [ ] Recitation 5: Attack Lab and Stacks                             (1.25h)
    - [ ] Lecture 09: Machine-Level Programming V: Advanced Topics        (2h)
    - [ ] Lecture 10: Program Optimization                                (2h)
    - [ ] Recitation 6: C Review                                          (1.5h)
    - [ ] Lecture 11: The Memory Hierachy                                 (2h)
    - [ ] Lecture 12: Cache Memories                                      (2h)
    - [ ] Recitation 7: Cache Lab and Blocking                            (1.5h)
    - [ ] Lecture 13: Linking                                             (1.5h)
    - [ ] Lecture 14: Exceptional Control Flow: Exceptions and Processes  (2h)
    - [ ] Recitation 8: Exam Review                                       (1.25h)
    - [ ] Lecture 15: Exceptional Control Flow: Signals and Nonlocal Jumps (2h)
    - [ ] Lecture 16: System Level I/O                                    (2h)
    - [ ] Recitation 9: Shell Lab, Processes, and Signals, and I/O        (1.5h)
    - [ ] Lecture 17: Virtual Memory: Concepts                            (2h)
    - [ ] Lecture 18: Virtual Memory: Systems                             (2h)
    - [ ] Recitation 10: Virtual Memory                                   (1.5h)
    - [ ] Lecture 18: Virtual Memory: Systems                             (2h)
    - [ ] Lecture 19: Dynamic Memory Allocation: Basic Concepts           (2h)
    - [ ] Lecture 20: Dynamic Memory Allocation: Advanced Concepts        (2h)
    - [ ] Recitation 11: Malloc Lab                                       (1.5h)
    - [ ] Lecture 21: Network Programming: Part 1                         (2h)
    - [ ] Lecture 22: Network Programming: Part II                        (2h)
    - [ ] Recitation 12: Debugging Malloc Lab                             (1.5h)
    - [ ] Lecture 23: Concurrent Programming                              (2h)
    - [ ] Lecture 24: Synchronization: Basics                             (1.5h)
    - [ ] Recitation 13: Proxy Lab                                        (1.5h)
    - [ ] Lecture 25: Synchronization: Advanced                           (2h)
    - [ ] Recitation 14: Synchronization                                  (1.5h)
    - [ ] Lecture 26: Thread-Level Parallelism                            (2h)
    - [ ] Lecture 27: Future of Computing                                 (2h)
    - [ ] Recitation 15: Exam review                                      (1h)
