#+AUTHOR: Bhavin Gandhi
#+EMAIL: bhavin7392@gmail.com
#+TAGS: read write dev ops event meeting # Need to be category
* GOALS
** Programming Languages
*** Go
** GNU Emacs
** GNU/Linux
** Containers
*** Namespaces
*** Control Groups
*** Container run times
**** Read about existing run times
*** Kubernetes
**** Production Kubernetes [0/16]
     :PROPERTIES:
     :ESTIMATED: 79
     :ACTUAL:
     :OWNER:    bhavin192
     :ID:       READ.1622393322
     :TASKID:   READ.1622393322
     :END:
     https://learning.oreilly.com/library/view/production-kubernetes/9781492092292/
     - [ ] 10. Identity                         (  7h)
     - [ ] 11. Building Platform Services       (  6h)
     - [ ] 12. Multitenancy                     (  4h)
     - [ ] 13. Autoscaling                      (  3h)
     - [ ] 14. Application Considerations       (4.5h)
     - [ ] 15. Software Supply Chain            (4.5h)
     - [ ] 16. Platform Abstractions            (3.5h)

** Site Reliability Engineering
** Attend meetups
** Write blog posts
* PLAN
